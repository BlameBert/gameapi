package com.berttowne.gameapi.mysql;

public interface Callback<V> {

    void call(V result);

    V get();

}