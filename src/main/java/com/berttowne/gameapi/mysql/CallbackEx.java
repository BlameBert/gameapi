package com.berttowne.gameapi.mysql;

public interface CallbackEx<V, T> {

    void call(V result, T thrown);

    V get();

}