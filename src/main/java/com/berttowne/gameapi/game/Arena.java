package com.berttowne.gameapi.game;

import com.berttowne.gameapi.util.MsgUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public abstract class Arena {

    private Plugin plugin;
    private FileConfiguration config;

    private String internalName;
    private String displayName;
    private String description;
    private String version;
    private String authors;

    private String world;

    private Location p1;
    private Location p2;
    private Location center;

    private ArrayList<Location> spawns;

    public Arena(Plugin plugin, String internalName) {
        this.plugin = plugin;
        this.config = YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder() + "/arenas/" + internalName + ".yml"));

        this.internalName = internalName;
        this.displayName = ChatColor.translateAlternateColorCodes('&', config.getString("DisplayName", "Arena"));
        this.authors = ChatColor.translateAlternateColorCodes('&', config.getString("Authors", "People"));
        this.description = ChatColor.translateAlternateColorCodes('&', config.getString("Description", "Insert stuff here"));
        this.version = ChatColor.translateAlternateColorCodes('&', config.getString("Version", "1.0"));
        this.spawns = new ArrayList<>();

        this.world = config.getString("WorldName", "");

        if (world.equals("")) {
            Bukkit.broadcast(MsgUtil.MsgType.WARNING.getPrefix() + "ERROR! " + ChatColor.GRAY + "World name not specified.", "gameapi.admin");
        }

        String p1String = config.getString("Locations.P1", "");

        if (p1String.equals("")) {
            Bukkit.broadcast(MsgUtil.MsgType.WARNING.getPrefix() + "ERROR! " + ChatColor.GRAY + "Min Location not specified.", "gameapi.admin");
        } else {
            String[] elements = p1String.split(",");
            this.p1 = new Location(Bukkit.getWorld(elements[0]),Double.valueOf(elements[1]),
                    Double.valueOf(elements[2]), Double.valueOf(elements[3]));
        }

        String p2String = config.getString("Locations.P2", "");

        if (p2String.equals("")) {
            Bukkit.broadcast(MsgUtil.MsgType.WARNING.getPrefix() + "ERROR! " + ChatColor.GRAY + "Max Location not specified.", "gameapi.admin");
        } else {
            String[] elements = p2String.split(",");
            this.p2 = new Location(Bukkit.getWorld(elements[0]),Double.valueOf(elements[1]),
                    Double.valueOf(elements[2]), Double.valueOf(elements[3]));
        }

        String centerString = config.getString("Locations.Center", "");

        if (centerString.equals("")) {
            Bukkit.broadcast(MsgUtil.MsgType.WARNING.getPrefix() + "ERROR! " + ChatColor.GRAY + "Center Location not specified.", "gameapi.admin");
        } else {
            String[] elements = centerString.split(",");
            this.center = new Location(Bukkit.getWorld(elements[0]),Double.valueOf(elements[1]),
                    Double.valueOf(elements[2]), Double.valueOf(elements[3]));
        }
    }

    @SuppressWarnings("unchecked")
    public void setupSpawns(){
        List<String> spawn = (List<String>) config.getList("Locations.Spawns");

        if (spawn != null) {
            for (String s : spawn) {
                String[] elements = s.split(",");
                spawns.add(new Location(Bukkit.getWorld(elements[0]),Double.valueOf(elements[1]),
                        Double.valueOf(elements[2]), Double.valueOf(elements[3])));
            }
        }
    }

    public FileConfiguration getConfig() {
        return config;
    }

    public void setConfig(FileConfiguration config) {
        this.config = config;
    }

    public String getInternalName() {
        return internalName;
    }

    public void setInternalName(String internalName) {
        this.internalName = internalName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public String getWorld() {
        return world;
    }

    public void setWorld(String world) {
        this.world = world;
    }

    public Location getP1() {
        return p1;
    }

    public void setP1(Location p1) {
        this.p1 = p1;
    }

    public Location getP2() {
        return p2;
    }

    public void setP2(Location p2) {
        this.p2 = p2;
    }

    public Location getCenter() {
        return center;
    }

    public void setCenter(Location center) {
        this.center = center;
    }

    public ArrayList<Location> getSpawns() {
        return spawns;
    }

    public void setSpawns(ArrayList<Location> spawns) {
        this.spawns = spawns;
    }

}