package com.berttowne.gameapi.game;

import com.berttowne.gameapi.Core;
import com.berttowne.gameapi.player.GamePlayer;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class Team {

    private String name;
    private Color color;
    private ChatColor chatColor;

    private List<GamePlayer> players;

    public Team(String name, Color color, ChatColor chatColor) {
        this.name = name;
        this.color = color;
        this.chatColor = chatColor;
        this.players = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public Color getColor() {
        return color;
    }

    public ChatColor getChatColor() {
        return chatColor;
    }

    public List<GamePlayer> getPlayers() {
        return players;
    }

    public boolean addPlayer(Player player) {
        GamePlayer p = Core.getPlayerManager().getPlayer(player.getName());

        if (!this.players.contains(p)) {
            players.add(p);
            return true;
        }

        return false;
    }

    public boolean removePlayer(Player player) {
        GamePlayer p = Core.getPlayerManager().getPlayer(player.getName());

        if (this.players.contains(p)) {
            players.remove(p);
            return true;
        }

        return false;
    }

    public boolean hasPlayer(Player player) {
        GamePlayer p = Core.getPlayerManager().getPlayer(player.getName());

        return this.players.contains(p);
    }

}