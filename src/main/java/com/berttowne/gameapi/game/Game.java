package com.berttowne.gameapi.game;

import com.berttowne.gameapi.Core;
import com.berttowne.gameapi.cmd.Command;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Game {

    private String name;
    private String internalName;
    private String prefix;

    private GameState currentGameState;

    private FileConfiguration configFile;

    private List<String> description;
    private List<String> players;
    private List<GameState> gameStates;
    private List<Team> teams;
    private List<Command> commands;
    private List<Arena> arenas;

    private int minPlayers;
    private int maxPlayers;
    private int currentPlayers;

    public Game(String name, String prefix, List<String> description, int minPlayers, int maxPlayers) {
        this.name = name;
        this.internalName = ChatColor.stripColor(name.replaceAll(" ", "_").toLowerCase());

        this.configFile = YamlConfiguration.loadConfiguration(new File(Core.getPlugin().getDataFolder() + "/" + internalName
                + "/config.yml"));

        this.prefix = configFile.getString("Settings.Prefix", prefix);
        this.description = description;
        this.minPlayers = configFile.getInt("Settings.MinPlayers", minPlayers);
        this.maxPlayers = configFile.getInt("Settings.MaxPlayers", maxPlayers);

        this.currentPlayers = 0;

        this.description = description;
        this.players = new ArrayList<>();
        this.gameStates = new ArrayList<>();
        this.teams = new ArrayList<>();
        this.commands = new ArrayList<>();
        this.arenas = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public String getPrefix() {
        return prefix;
    }

    public GameState getCurrentGameState() {
        return currentGameState;
    }

    public void setCurrentGameState(GameState currentGameState) {
        this.currentGameState = currentGameState;
    }

    public List<String> getDescription() {
        return description;
    }

    public List<GameState> getGameStates() {
        return gameStates;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public List<Command> getCommands() {
        return commands;
    }

    public List<Arena> getArenas() {
        return arenas;
    }

    public int getMinPlayers() {
        return minPlayers;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public int getCurrentPlayers() {
        return currentPlayers;
    }

}