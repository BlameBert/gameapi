package com.berttowne.gameapi.game;

import com.berttowne.gameapi.player.Rank;

public class GameState {

    private String name;
    private Rank minRank;
    private int id;
    private boolean joinable;

    public GameState(String name, Rank minRank, int id, boolean joinable) {
        this.name = name;
        this.minRank = minRank;
        this.id = id;
        this.joinable = joinable;
    }

    public Rank getMinRank() {
        return minRank;
    }

    public boolean isJoinable() {
        return joinable;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

}