package com.berttowne.gameapi.achievement;

import com.berttowne.gameapi.Core;
import com.berttowne.gameapi.gui.GUI;
import com.berttowne.gameapi.util.GlowUtil;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class AchievementsGUI extends GUI {

    private Player player;
    private String game;

    public AchievementsGUI(Player player, String game) {
        super("Achievements", 54);

        this.player = player;
        this.game = game;
    }

    @Override
    protected void onPlayerClick(InventoryClickEvent event) {
        event.setCancelled(true);
    }

    @Override
    protected void populate() {
        for (int i = 0; i < inventory.getSize(); i++) {
            if (i < 9 || i > 44 || (i + 1) % 9 == 0 || i % 9 == 0) {
                inventory.setItem(i, SPACER);
            }
        }

        ItemStack header = new ItemStack(Material.BOOK_AND_QUILL);
        ItemMeta meta = header.getItemMeta();
        meta.setDisplayName("§e§lACHIEVEMENTS");
        header.setItemMeta(meta);
        inventory.setItem(4, header);

        for (Achievement achievement : Core.getAchievementHandler().getAchievements()) {
            if (this.game.equalsIgnoreCase(achievement.getGame()) || this.game.equalsIgnoreCase("all")) {
                if (Core.getAchievementHandler().hasAchievement(player, achievement)) {
                    ItemStack itemStack = new ItemStack(Material.EMERALD_BLOCK);
                    ItemMeta itemMeta = itemStack.getItemMeta();
                    itemMeta.setDisplayName("§a§l" + achievement.getName());
                    itemMeta.setLore(Arrays.asList(
                            " ",
                            "§f" + achievement.getGame() + " achievement",
                            " ",
                            "§7§o" + achievement.getDescription(),
                            " ",
                            "§aReward: §f" + achievement.getReward() + " souls"
                    ));
                    itemStack.setItemMeta(itemMeta);
                    GlowUtil.addGlow(itemStack);

                    inventory.addItem(itemStack);
                } else {
                    ItemStack itemStack = new ItemStack(Material.REDSTONE_BLOCK);
                    ItemMeta itemMeta = itemStack.getItemMeta();
                    itemMeta.setDisplayName("§c§l" + achievement.getName());
                    itemMeta.setLore(Arrays.asList(
                            " ",
                            "§7§oYou have not received this achievement!",
                            "§7§oComplete it to see its details."
                    ));
                    itemStack.setItemMeta(itemMeta);

                    inventory.addItem(itemStack);
                }
            }
        }
    }

}