package com.berttowne.gameapi.achievement;

import com.berttowne.gameapi.Core;
import com.berttowne.gameapi.player.GamePlayer;
import com.berttowne.gameapi.util.BountifulAPI;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.logging.Level;

public class AchievementHandler {

    private ArrayList<Achievement> achievements;

    public AchievementHandler() {
        this.achievements = new ArrayList<>();
    }

    public String getName(int id) {
        FutureTask<String> task = new FutureTask<>(() -> {
            Connection con = Core.getAcon();
            String sql = "SELECT name FROM achievements WHERE id = ?";

            try {
                PreparedStatement stmt = con.prepareStatement(sql);
                stmt.setInt(1, id);
                ResultSet set = stmt.executeQuery();

                if (set.next()) {
                    return set.getString(1);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        });

        new Thread(task).start();

        try {
            return task.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getGame(int id) {
        FutureTask<String> task = new FutureTask<>(() -> {
            Connection con = Core.getAcon();
            String sql = "SELECT game FROM achievements WHERE id = ?";

            try {
                PreparedStatement stmt = con.prepareStatement(sql);
                stmt.setInt(1, id);
                ResultSet set = stmt.executeQuery();

                if (set.next()) {
                    return set.getString(1);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        });

        new Thread(task).start();

        try {
            return task.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getDesciption(int id) {
        FutureTask<String> task = new FutureTask<>(() -> {
            Connection con = Core.getAcon();
            String sql = "SELECT description FROM achievements WHERE id = ?";

            try {
                PreparedStatement stmt = con.prepareStatement(sql);
                stmt.setInt(1, id);
                ResultSet set = stmt.executeQuery();

                if (set.next()) {
                    return set.getString(1);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        });

        new Thread(task).start();

        try {
            return task.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    public int getReward(int id) {
        FutureTask<Integer> task = new FutureTask<>(() -> {
            Connection con = Core.getAcon();
            String sql = "SELECT reward FROM achievements WHERE id = ?";

            try {
                PreparedStatement stmt = con.prepareStatement(sql);
                stmt.setInt(1, id);
                ResultSet set = stmt.executeQuery();

                if (set.next()) {
                    return set.getInt(1);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return 0;
        });

        new Thread(task).start();

        try {
            return task.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int getID(Achievement achievement) {
        FutureTask<Integer> task = new FutureTask<>(() -> {
            Connection con = Core.getAcon();
            String sql = "SELECT id FROM achievements WHERE name = ?";

            try {
                PreparedStatement stmt = con.prepareStatement(sql);
                stmt.setString(1, achievement.getName());
                ResultSet set = stmt.executeQuery();

                if (set.next()) {
                    return set.getInt(1);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return 0;
        });

        new Thread(task).start();

        try {
            return task.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public Achievement getAchievement(int id) {
        if (getName(id) != null && getGame(id) != null && getDesciption(id) != null && getReward(id) != 0) {
            return new Achievement(getName(id), getGame(id), getDesciption(id), getReward(id));
        }

        return null;
    }

    public void registerAchievement(Achievement achievement) {
        if (getAchievement(getID(achievement)) == null) {
            new BukkitRunnable() {
                @Override
                public void run() {
                    Connection con = Core.getAcon();
                    String sql = "INSERT INTO `achievements`(name, game, description, reward) VALUES (?, ?, ?, ?)";

                    try {
                        PreparedStatement stmt = con.prepareStatement(sql);
                        stmt.setString(1, achievement.getName());
                        stmt.setString(2, achievement.getGame());
                        stmt.setString(3, achievement.getDescription());
                        stmt.setInt(4, achievement.getReward());
                        stmt.executeUpdate();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.runTaskAsynchronously(Core.getPlugin());

            Core.getPlugin().getLogger().log(Level.INFO, "Registered achievement - " + achievement.getName());
        } else {
            Core.getPlugin().getLogger().log(Level.INFO, "Loaded achievement - " + achievement.getName());
        }

        this.achievements.add(achievement);
    }

    public void award(Player player, Achievement achievement) {
        if (!hasAchievement(player, achievement)) {
            new BukkitRunnable() {
                @Override
                public void run() {
                    Connection con = Core.getAcon();
                    String sql = "INSERT INTO `players`(uuid, achievement_id) VALUES (?, ?)";

                    try {
                        PreparedStatement stmt = con.prepareStatement(sql);
                        stmt.setString(1, player.getUniqueId().toString());
                        stmt.setInt(2, getID(achievement));
                        stmt.executeUpdate();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.runTaskAsynchronously(Core.getPlugin());

            GamePlayer p = Core.getPlayerManager().getPlayer(player.getName());
            p.setShards(p.getShards() + achievement.getReward()); // TODO: Fix balances

            BountifulAPI.sendTitle(player, 20, 20 * 3, 20, "§2§l§kii§a§l ACHIEVEMENT GET! §2§l§kii", "§f§l§o" + achievement.getDescription());
            BountifulAPI.sendActionBar(player, "§2§l+ §a§l" + achievement.getReward() + " SHARDS!", 5 * 20);

            player.sendMessage("§2§l╔════════════════════════════");
            player.sendMessage("§2§l║");
            player.sendMessage("§2§l║   §9§l§kii§a§l ACHIEVEMENT GET! §9§l§kii");
            player.sendMessage("§2§l║");
            player.sendMessage("§2§l║   §f§l" + achievement.getName());
            player.sendMessage("§2§l║   §7§o" + achievement.getDescription());
            player.sendMessage("§2§l║");
            player.sendMessage("§2§l║    §8§l+ §e§l" + achievement.getReward() + " SHARDS");
            player.sendMessage("§2§l║");
            player.sendMessage("§2§l╚════════════════════════════");

            player.playSound(player.getLocation(), Sound.ENTITY_WITHER_SPAWN, 1f, 1f);
            player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1f, 1f);
            player.playSound(player.getLocation(), Sound.ENTITY_WOLF_HOWL, 1f, 1f);
        } else {
            Core.getPlugin().getLogger().log(Level.INFO, "Player already has the achievement - " + achievement.getName());
        }
    }

    public boolean hasAchievement(Player player, Achievement achievement) {
        FutureTask<Boolean> task = new FutureTask<>(() -> {
            Connection con = Core.getAcon();
            String sql = "SELECT achievement_id FROM players WHERE uuid = ?";

            try {
                PreparedStatement stmt = con.prepareStatement(sql);
                stmt.setString(1, player.getUniqueId().toString());
                ResultSet set = stmt.executeQuery();

                if (set.next()) {
                    return getAchievement(getID(achievement)) != null;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        });

        new Thread(task).start();

        try {
            return task.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return false;
    }

    public ArrayList<Achievement> getAchievements() {
        return achievements;
    }

}