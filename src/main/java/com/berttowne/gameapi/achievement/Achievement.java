package com.berttowne.gameapi.achievement;

public class Achievement {

    private String name;
    private String game;
    private String description;
    private int reward;

    public Achievement(String name, String game, String description, int reward) {
        this.name = name;
        this.game = game;
        this.description = description;
        this.reward = reward;
    }

    public String getName() {
        return name;
    }

    public String getGame() {
        return game;
    }

    public String getDescription() {
        return description;
    }

    public int getReward() {
        return reward;
    }

}