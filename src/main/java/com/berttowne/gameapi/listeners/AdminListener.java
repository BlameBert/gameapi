package com.berttowne.gameapi.listeners;

import com.berttowne.gameapi.Core;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;

public class AdminListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerBedEnter(PlayerBedEnterEvent event) {
        if (Core.getPlayerManager().getPlayer(event.getPlayer().getName()) == null) return;
        if (Core.getPlayerManager().createPlayer(event.getPlayer().getName()).isAdminMode()) {
            event.setCancelled(false);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerBucketEmpty(PlayerBucketEmptyEvent event) {
        if (Core.getPlayerManager().getPlayer(event.getPlayer().getName()) == null) return;
        if (Core.getPlayerManager().createPlayer(event.getPlayer().getName()).isAdminMode()) {
            event.setCancelled(false);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerBucketFill(PlayerBucketFillEvent event) {
        if (Core.getPlayerManager().getPlayer(event.getPlayer().getName()) == null) return;
        if (Core.getPlayerManager().createPlayer(event.getPlayer().getName()).isAdminMode()) {
            event.setCancelled(false);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerDropItem(PlayerDropItemEvent event) {
        if (Core.getPlayerManager().getPlayer(event.getPlayer().getName()) == null) return;
        if (Core.getPlayerManager().createPlayer(event.getPlayer().getName()).isAdminMode()) {
            event.setCancelled(false);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerPickupItem(PlayerPickupItemEvent event) {
        if (Core.getPlayerManager().getPlayer(event.getPlayer().getName()) == null) return;
        if (Core.getPlayerManager().createPlayer(event.getPlayer().getName()).isAdminMode()) {
            event.setCancelled(false);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerPickupArrow(PlayerPickupArrowEvent event) {
        if (Core.getPlayerManager().getPlayer(event.getPlayer().getName()) == null) return;
        if (Core.getPlayerManager().createPlayer(event.getPlayer().getName()).isAdminMode()) {
            event.setCancelled(false);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerEditBook(PlayerEditBookEvent event) {
        if (Core.getPlayerManager().getPlayer(event.getPlayer().getName()) == null) return;
        if (Core.getPlayerManager().createPlayer(event.getPlayer().getName()).isAdminMode()) {
            event.setCancelled(false);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerFish(PlayerFishEvent event) {
        if (Core.getPlayerManager().getPlayer(event.getPlayer().getName()) == null) return;
        if (Core.getPlayerManager().createPlayer(event.getPlayer().getName()).isAdminMode()) {
            event.setCancelled(false);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (Core.getPlayerManager().getPlayer(event.getPlayer().getName()) == null) return;
        if (Core.getPlayerManager().createPlayer(event.getPlayer().getName()).isAdminMode()) {
            event.setCancelled(false);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerItemConsume(PlayerItemConsumeEvent event) {
        if (Core.getPlayerManager().getPlayer(event.getPlayer().getName()) == null) return;
        if (Core.getPlayerManager().createPlayer(event.getPlayer().getName()).isAdminMode()) {
            event.setCancelled(false);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerPortalEvent(PlayerPortalEvent event) {
        if (Core.getPlayerManager().getPlayer(event.getPlayer().getName()) == null) return;
        if (Core.getPlayerManager().createPlayer(event.getPlayer().getName()).isAdminMode()) {
            event.setCancelled(false);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerShearEntity(PlayerShearEntityEvent event) {
        if (Core.getPlayerManager().getPlayer(event.getPlayer().getName()) == null) return;
        if (Core.getPlayerManager().createPlayer(event.getPlayer().getName()).isAdminMode()) {
            event.setCancelled(false);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
        if (!(event.getEntity() instanceof Player)) return;
        if (Core.getPlayerManager().getPlayer(event.getEntity().getName()) == null) return;
        if (Core.getPlayerManager().createPlayer(event.getDamager().getName()).isAdminMode()) {
            event.setCancelled(false);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onEntityDamage(EntityDamageEvent event) {
        if (!(event.getEntity() instanceof Player)) return;
        if (Core.getPlayerManager().getPlayer(event.getEntity().getName()) == null) return;
        if (Core.getPlayerManager().createPlayer(event.getEntity().getName()).isAdminMode()) {
            event.setCancelled(false);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onInventoryClick(InventoryClickEvent event) {
        if (Core.getPlayerManager().getPlayer(event.getWhoClicked().getName()) == null) return;
        if (Core.getPlayerManager().createPlayer(event.getWhoClicked().getName()).isAdminMode()) {
            event.setCancelled(false);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockPlace(BlockPlaceEvent event) {
        if (Core.getPlayerManager().getPlayer(event.getPlayer().getName()) == null) return;
        if (Core.getPlayerManager().createPlayer(event.getPlayer().getName()).isAdminMode()) {
            event.setCancelled(false);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockBreak(BlockBreakEvent event) {
        if (Core.getPlayerManager().getPlayer(event.getPlayer().getName()) == null) return;
        if (Core.getPlayerManager().createPlayer(event.getPlayer().getName()).isAdminMode()) {
            event.setCancelled(false);
        }
    }

}