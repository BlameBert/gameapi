package com.berttowne.gameapi.listeners;

import com.berttowne.gameapi.Core;
import com.berttowne.gameapi.npc.PlayerNPC;
import com.berttowne.gameapi.player.GamePlayer;
import com.berttowne.gameapi.server.ServerData;
import com.berttowne.gameapi.util.BountifulAPI;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.UUID;

public class JoinListener implements Listener {

    @EventHandler
    public void onPlayerLogin(PlayerLoginEvent event) {
        GamePlayer player = Core.getPlayerManager().createPlayer(event.getPlayer().getName());

        if (player == null) {
            event.disallow(PlayerLoginEvent.Result.KICK_OTHER, "§4§lERROR RETRIEVING PLAYER DATA!" +
                    "\n§cPlease tweet @Blame_Bert with a screenshot" +
                    "\n§cof this message and your in-game name!");
        } else {
            if (Core.getPlayerManager().isBanned(player.getName())) {
                event.disallow(PlayerLoginEvent.Result.KICK_BANNED, Core.getPlayerManager().getBanString(player.getName()));
            } else {
                if (Core.getPlayerManager().isMuted(player.getName())) {
                    player.message(Core.getPlayerManager().getMuteString(player.getName()));
                }
            }

            if (player.getRank().getId() < Core.getServerData().getMinRank().getId()) {
                event.disallow(PlayerLoginEvent.Result.KICK_OTHER, "§4§lERROR! §cYou need to be a(n)\n§c"
                        + Core.getServerData().getMinRank().getName() + "+ in order to join!");
            }

            if (!Core.getServerData().getState().isJoinable()) {
                event.disallow(PlayerLoginEvent.Result.KICK_OTHER, "§4§lERROR! §cYou cannot join\n§c"
                        + "during " + Core.getServerData().getState().getName() + "!");
            }
        }
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        GamePlayer player = Core.getPlayerManager().createPlayer(event.getPlayer().getName());

        for (PlayerNPC playerNPC : PlayerNPC.getRegisteredNPCs()) {
            playerNPC.display(event.getPlayer());
        }

        if (player != null) {
            if (player.getRank() != null) {
                event.getPlayer().setPlayerListName(player.getRank().getPrefix() + player.getName());
            }

            Bukkit.getScheduler().scheduleSyncRepeatingTask(Core.getPlugin(), new BukkitRunnable() {
                @Override
                public void run() {
                    if (Bukkit.getPlayer(UUID.fromString(player.getUUID())) != null) {
                        if (player.isAdminMode()) {
                            BountifulAPI.sendActionBar(event.getPlayer(), "§f§lYou are in §c§lADMIN MODE!");
                        }
                    }
                }
            }, 0L, 10L);
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        GamePlayer player = Core.getPlayerManager().createPlayer(event.getPlayer().getName());

        if (Core.getPlayerManager().getOnlinePlayers().contains(player)) {
            Core.getPlayerManager().getOnlinePlayers().remove(player);
        }
    }

}