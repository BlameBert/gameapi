package com.berttowne.gameapi.listeners;

import com.berttowne.gameapi.Core;
import com.berttowne.gameapi.player.GamePlayer;
import com.berttowne.gameapi.player.Rank;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatListener implements Listener {

    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent event) {
        Bukkit.getScheduler().runTaskAsynchronously(Core.getPlugin(), () -> {
            GamePlayer player = Core.getPlayerManager().getPlayer(event.getPlayer().getName());

            if (!player.isMuted()) {
                event.setFormat(player.getRank().getPrefix() + player.getName() + "§8: " + player.getRank().getChatColor() + event.getMessage());
            } else {
                event.setCancelled(true);
                player.message(Core.getPlayerManager().getMuteString(player.getName()));
            }

            if (player.getRank().getId() >= Rank.ADMINISTRATOR.getId()) {
                if (event.getMessage().startsWith("!")) {
                    event.setCancelled(true);

                    Bukkit.broadcastMessage("§4§l╔════════════════════════════");
                    Bukkit.broadcastMessage("§4§l║");
                    Bukkit.broadcastMessage("§4§l║   §c§l" + event.getPlayer().getName() + " says...");
                    Bukkit.broadcastMessage("§4§l║   §6§o" + event.getMessage().substring(1));
                    Bukkit.broadcastMessage("§4§l║");
                    Bukkit.broadcastMessage("§4§l╚════════════════════════════");
                }
            }
        });
    }

}