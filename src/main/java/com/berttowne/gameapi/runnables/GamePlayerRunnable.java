package com.berttowne.gameapi.runnables;

import com.berttowne.gameapi.Core;
import com.berttowne.gameapi.mysql.Callback;
import com.berttowne.gameapi.player.GamePlayer;
import com.berttowne.gameapi.player.Rank;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class GamePlayerRunnable extends BukkitRunnable {

    private Callback<GamePlayer> gamePlayerCallback;
    private Callback<Rank> rankCallback;

    private Player p;

    public GamePlayerRunnable(Player p) {
        this.p = p;
        this.run();
    }

    public GamePlayer getPlayer() {
        return gamePlayerCallback.get();
    }

    public Rank getRank() {
        return rankCallback.get();
    }

    @Override
    public void run() {
        this.gamePlayerCallback = new Callback<GamePlayer>() {
            GamePlayer player;

            @Override
            public void call(GamePlayer result) {
                player = Core.getPlayerManager().createPlayer(p.getName());
            }

            @Override
            public GamePlayer get() {
                return player;
            }
        };

        this.rankCallback = new Callback<Rank>() {
            Rank rank;

            @Override
            public void call(Rank result) {
                rank = gamePlayerCallback.get().getRank();
            }

            @Override
            public Rank get() {
                return rank;
            }
        };
    }

}