package com.berttowne.gameapi.runnables;

import com.berttowne.gameapi.Core;
import com.berttowne.gameapi.player.GamePlayer;
import com.berttowne.gameapi.player.Rank;
import org.bukkit.Bukkit;
import org.bukkit.Particle;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class RankParticles extends BukkitRunnable {

    @Override
    public void run() {
        for (GamePlayer player : Core.getPlayerManager().getOnlinePlayers()) {
            if (player.getRank().getId() == Rank.OWNER.getId()) {
                Player p = Bukkit.getPlayer(player.getName());

                if (p != null) {
                    p.getWorld().spawnParticle(Particle.REDSTONE, p.getEyeLocation(), 10, 1.0, 1.0, 1.0, 3.0);
                }
            }
        }
    }

}