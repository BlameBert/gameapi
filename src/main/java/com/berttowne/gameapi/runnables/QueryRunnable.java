package com.berttowne.gameapi.runnables;

import com.berttowne.gameapi.mysql.CallbackEx;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class QueryRunnable extends BukkitRunnable {

    private final Connection connection;
    private final String statement;
    private final CallbackEx<ResultSet, SQLException> callback;

    public QueryRunnable(Connection connection, String statement, CallbackEx<ResultSet, SQLException> callback) {
        this.statement = statement;
        this.callback = callback;
        this.connection = connection;
    }

    @Override
    public void run() {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            preparedStatement = connection.prepareStatement(statement);
            resultSet = preparedStatement.executeQuery();
            callback.call(resultSet, null);
        } catch (SQLException ex) {
            callback.call(null, ex);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public String getStatement() {
        return statement;
    }

    public CallbackEx<ResultSet, SQLException> getCallback() {
        return callback;
    }

}