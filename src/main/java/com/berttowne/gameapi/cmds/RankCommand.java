package com.berttowne.gameapi.cmds;

import com.berttowne.gameapi.Core;
import com.berttowne.gameapi.cmd.Command;
import com.berttowne.gameapi.player.GamePlayer;
import com.berttowne.gameapi.player.Rank;
import com.berttowne.gameapi.util.MsgUtil;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class RankCommand extends Command {

    public RankCommand() {
        super(Core.getCommandHandler(), "rank");
    }

    @Override
    public void execute(Player player, String... args) {
        GamePlayer p = Core.getPlayerManager().createPlayer(player.getName());

        if (p.getRank().getId() < 3) {
            p.message(MsgUtil.MsgType.DANGER, "You do not have permission to use this command!");
            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BASS, 1f, 1f);
            return;
        }

        if (args.length != 2) {
            p.message(MsgUtil.MsgType.DANGER, "Invalid arguments! Use /rank <player> <rank id>");
            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BASS, 1f, 1f);
            return;
        }

        GamePlayer target = Core.getPlayerManager().getPlayer(args[0]);

        if (target != null) {
            if (Rank.getById(Integer.parseInt(args[1])) != null) {
                Rank rank = Rank.getById(Integer.parseInt(args[1]));

                if (p.getRank().getId() < rank.getId()) {
                    p.message(MsgUtil.MsgType.DANGER, "You cannot set someone's rank to one that is higher than yours!");
                    player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BASS, 1f, 1f);
                    return;
                }

                target.setRank(rank);
                target.message(MsgUtil.MsgType.SUCCESS, "Your rank has been changed to §7§o" + rank.getName().toLowerCase() + "§a!");
                player.sendMessage(MsgUtil.MsgType.SUCCESS.getPrefix() + "Changed " + player.getName() + "'s rank to §a§o"
                        + rank.getName().toLowerCase() + "§a!");

                if (Bukkit.getPlayer(target.getName()).isOnline()) {
                    Bukkit.getPlayer(target.getName()).setPlayerListName(target.getRank().getPrefix() + target.getName());
                }
            } else {
                player.sendMessage(MsgUtil.MsgType.DANGER.getPrefix() + "That rank does not exist!");
                player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BASS, 1f, 1f);
            }
        } else {
            p.message(MsgUtil.MsgType.DANGER, "Player §f" + args[0] + " §cnot found! Have they joined the server before?");
            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BASS, 1f, 1f);
        }
    }

}