package com.berttowne.gameapi.cmds.punishments;

import com.berttowne.gameapi.Core;
import com.berttowne.gameapi.cmd.Command;
import com.berttowne.gameapi.player.GamePlayer;
import com.berttowne.gameapi.util.MsgUtil;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class UnbanCommand extends Command {

    public UnbanCommand() {
        super(Core.getCommandHandler(), "unban");
    }

    @Override
    public void execute(Player player, String... args) {
        GamePlayer p = Core.getPlayerManager().createPlayer(player.getName());

        if (p.getRank().getId() < 2) {
            p.message(MsgUtil.MsgType.DANGER, "You do not have permission to use this command!");
            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BASS, 1f, 1f);
            return;
        }

        if (args.length != 1) {
            p.message(MsgUtil.MsgType.DANGER, "Invalid arguments! Use /unban <player>");
            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BASS, 1f, 1f);
            return;
        }

        GamePlayer target = Core.getPlayerManager().getPlayer(args[0]);

        if (target != null) {
            if (target.isBanned()) {
                Core.getPlayerManager().unban(target.getName(), player.getName());
            } else {
                p.message(MsgUtil.MsgType.DANGER, "Player §f" + args[0] + " §cnot is not banned!");
                player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BASS, 1f, 1f);
            }
        } else {
            p.message(MsgUtil.MsgType.DANGER, "Player §f" + args[0] + " §cnot found! Have they joined the server before?");
            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BASS, 1f, 1f);
        }
    }

}