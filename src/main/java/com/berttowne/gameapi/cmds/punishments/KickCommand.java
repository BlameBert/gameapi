package com.berttowne.gameapi.cmds.punishments;

import com.berttowne.gameapi.Core;
import com.berttowne.gameapi.cmd.Command;
import com.berttowne.gameapi.player.GamePlayer;
import com.berttowne.gameapi.util.MsgUtil;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class KickCommand extends Command {

    public KickCommand() {
        super(Core.getCommandHandler(), "kick");
    }

    @Override
    public void execute(Player player, String... args) {
        GamePlayer p = Core.getPlayerManager().createPlayer(player.getName());

        if (p.getRank().getId() < 2) {
            p.message(MsgUtil.MsgType.DANGER, "You do not have permission to use this command!");
            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BASS, 1f, 1f);
            return;
        }

        if (args.length < 2) {
            p.message(MsgUtil.MsgType.DANGER, "Invalid arguments! Use /kick <player> [reason]");
            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BASS, 1f, 1f);
            return;
        }

        GamePlayer target = Core.getPlayerManager().createPlayer(args[0]);

        StringBuilder reason = new StringBuilder();

        for (int i = 1; i < args.length; i++) {
            reason.append(args[i]).append(" ");
        }

        Core.getPlayerManager().kick(args[0], reason.toString(), player.getName());
    }

}