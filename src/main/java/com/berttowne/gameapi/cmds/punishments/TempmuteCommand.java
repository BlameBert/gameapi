package com.berttowne.gameapi.cmds.punishments;

import com.berttowne.gameapi.Core;
import com.berttowne.gameapi.cmd.Command;
import com.berttowne.gameapi.player.GamePlayer;
import com.berttowne.gameapi.util.MsgUtil;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public class TempmuteCommand extends Command {

    public TempmuteCommand() {
        super(Core.getCommandHandler(), "tempmute");
    }

    @Override
    public void execute(Player player, String... args) {
        GamePlayer p = Core.getPlayerManager().createPlayer(player.getName());

        if (p.getRank().getId() < 2) {
            p.message(MsgUtil.MsgType.DANGER, "You do not have permission to use this command!");
            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BASS, 1f, 1f);
            return;
        }

        if (args.length < 9) {
            p.message(MsgUtil.MsgType.DANGER, "Invalid arguments! Use /tempmute <player> [years]y [months]m [days]d [hours]h [minutes]m " +
                    "[seconds]s [reason]");
            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BASS, 1f, 1f);
            return;
        }

        if (Core.getPlayerManager().isBanned(args[0])) {
            p.message(MsgUtil.MsgType.DANGER, "That player is already banned!");
            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BASS, 1f, 1f);
            return;
        }

        GamePlayer target = Core.getPlayerManager().createPlayer(args[0]);

        if (args[1].endsWith("y")) {
            if (args[2].endsWith("m")) {
                if (args[3].endsWith("d")) {
                    if (args[4].endsWith("h")) {
                        if (args[5].endsWith("m")) {
                            if (args[6].endsWith("s")) {
                                String banner = player.getName();

                                StringBuilder reason = new StringBuilder();
                                for (int i = 7; i < args.length; i++) {
                                    reason.append(args[i]).append(" ");
                                }

                                int years = Integer.parseInt(args[1].substring(0, args[1].length() - 1));
                                int months = Integer.parseInt(args[2].substring(0, args[2].length() - 1));
                                int days = Integer.parseInt(args[3].substring(0, args[3].length() - 1));
                                int hours = Integer.parseInt(args[1].substring(0, args[4].length() - 1));
                                int minutes = Integer.parseInt(args[2].substring(0, args[5].length() - 1));
                                int seconds = Integer.parseInt(args[3].substring(0, args[6].length() - 1));

                                Core.getPlayerManager().mute(args[0], reason.toString(), banner, getDuration(years, months, days, hours,
                                        minutes, seconds));
                            }
                        }
                    }
                }
            }
        }
    }

    private Duration getDuration(int years, int months, int days, int hours, int minutes, int seconds) {
        Date date = new Date();
        Instant instant = Instant.now();

        instant.plus(years, ChronoUnit.YEARS);
        instant.plus(months, ChronoUnit.MONTHS);
        instant.plus(days, ChronoUnit.DAYS);
        instant.plus(hours, ChronoUnit.HOURS);
        instant.plus(minutes, ChronoUnit.MINUTES);
        instant.plus(seconds, ChronoUnit.SECONDS);

        Date end = Date.from(instant);

        return Duration.between(date.toInstant(), end.toInstant());
    }

}