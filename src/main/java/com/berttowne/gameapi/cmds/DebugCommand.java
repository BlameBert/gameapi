package com.berttowne.gameapi.cmds;

import com.berttowne.gameapi.Core;
import com.berttowne.gameapi.achievement.Achievement;
import com.berttowne.gameapi.cmd.Command;
import com.berttowne.gameapi.gui.AnvilGUI;
import com.berttowne.gameapi.npc.PlayerNPC;
import com.berttowne.gameapi.npc.TitledPlayerNPC;
import com.berttowne.gameapi.player.GamePlayer;
import com.berttowne.gameapi.server.ServerGUI;
import com.berttowne.gameapi.time.Timer;
import com.berttowne.gameapi.util.BountifulAPI;
import com.berttowne.gameapi.util.MsgUtil;
import com.berttowne.gameapi.util.fetchers.UUIDFetcher;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.*;

import java.util.logging.Level;

public class DebugCommand extends Command {

    public DebugCommand() {
        super(Core.getCommandHandler(), "test");
    }

    @Override
    public void execute(Player player, String... args) {
        GamePlayer p = Core.getPlayerManager().createPlayer(player.getName());

        if (p.getRank().getId() < 3) {
            p.message(MsgUtil.MsgType.DANGER, "You do not have permission to use this command!");
            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BASS, 1f, 1f);
            return;
        }

        if (args.length != 1) {
            p.message(MsgUtil.MsgType.DANGER, "Invalid arguments! Use /test <feature>");
            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BASS, 1f, 1f);
            return;
        }

        if (args[0].equalsIgnoreCase("anvil")) {
            AnvilGUI anvilGUI = new AnvilGUI(Core.getPlugin(), player, "Say something...", ((player1, reply) -> {
                player.sendMessage(MsgUtil.MsgType.SUCCESS.getPrefix() + reply);
                return null;
            }));
        }

        if (args[0].equalsIgnoreCase("achievement")) {
            Achievement achievement = Core.getAchievementHandler().getAchievements().get(0);
            if (achievement != null) {
                Core.getAchievementHandler().award(player, achievement);
            } else {
                Core.getPlugin().getLogger().log(Level.SEVERE, "Achievement cannot be found!");
            }
        }

        if (args[0].equalsIgnoreCase("server")) {
            new ServerGUI().open(player);
        }

        if (args[0].equalsIgnoreCase("title")) {
            TestTitle testPlayerNPC = new TestTitle(player);

            testPlayerNPC.register(UUIDFetcher.getUUIDOf("Turqmelon"));
        }

        if (args[0].equalsIgnoreCase("player")) {
            TestPlayerNPC testPlayerNPC = new TestPlayerNPC(player);

            testPlayerNPC.register(UUIDFetcher.getUUIDOf("ElPollo"));
        }

        if (args[0].equalsIgnoreCase("despawn")) {
            PlayerNPC.despawnAll();
        }

        if (args[0].equalsIgnoreCase("timer")) {
            new TimerTest().start();
        }

    }

    private class TimerTest extends Timer {

        public TimerTest() {
            super(0, 0, 1);
        }

        @Override
        protected void onStart() {
            Bukkit.getOnlinePlayers().forEach(player -> {
                BountifulAPI.sendActionBar(player, ChatColor.BOLD +
                        this.getFormattedTimeWords(ChatColor.WHITE, ChatColor.LIGHT_PURPLE, false));
            });
        }

        @Override
        protected void onStop() {

        }

        @Override
        protected void onTime(int hours, int minutes, int seconds) {
            Bukkit.getOnlinePlayers().forEach(player -> {
                BountifulAPI.sendActionBar(player, ChatColor.BOLD +
                        this.getFormattedTimeWords(ChatColor.WHITE, ChatColor.LIGHT_PURPLE, false));
            });
        }

    }

    private class TestTitle extends TitledPlayerNPC {

        private Player player;

        public TestTitle(Player player) {
            super("§c ", player.getLocation(), true,
                    "§c§lTEST NPC",
                    " ",
                    "§7Will this work?",
                    "§7The world may never know!");

            this.player = player;
        }

        @Override
        protected void onRegister(PlayerNPC playerNPC) {
            player.sendMessage(MsgUtil.MsgType.SUCCESS.getPrefix() + "Successfully spawned NPC!");
        }

        @Override
        protected void onRightClick(PlayerNPC playerNPC, Player player) {
            player.sendMessage(MsgUtil.MsgType.SUCCESS.getPrefix() + "Bloop!");
        }

    }

    private class TestPlayerNPC extends PlayerNPC {

        private Player player;

        public TestPlayerNPC(Player player) {
            super("§4§lElPollo", player.getLocation(), true);

            this.player = player;
        }

        @Override
        protected void onRegister(PlayerNPC playerNPC) {
            player.sendMessage(MsgUtil.MsgType.SUCCESS.getPrefix() + "Successfully spawned NPC!");
        }

        @Override
        protected void onRightClick(PlayerNPC playerNPC, Player player) {
            player.sendMessage(MsgUtil.MsgType.SUCCESS.getPrefix() + "Bloop!");
        }

    }

}