package com.berttowne.gameapi.cmds;

import com.berttowne.gameapi.Core;
import com.berttowne.gameapi.player.GamePlayer;
import com.berttowne.gameapi.util.BountifulAPI;
import com.berttowne.gameapi.util.MsgUtil;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class AdminModeCommand extends com.berttowne.gameapi.cmd.Command {

    public AdminModeCommand() {
        super(Core.getCommandHandler(), "adminmode");
    }

    @Override
    public void execute(Player player, String... args) {
        GamePlayer p = Core.getPlayerManager().createPlayer(player.getName());

        if (p.getRank().getId() < 3) {
            p.message(MsgUtil.MsgType.DANGER, "You do not have permission to use this command!");
            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BASS, 1f, 1f);
            return;
        }

        p.setAdminMode(!p.isAdminMode());

        Bukkit.getScheduler().scheduleSyncRepeatingTask(Core.getPlugin(), new BukkitRunnable() {
            @Override
            public void run() {
                if (p.isAdminMode()) {
                    BountifulAPI.sendActionBar(player, "§f§lYou are in §c§lADMIN MODE!");
                } else {
                    this.cancel();
                }
            }
        }, 0L, 20L);

        for (Player pl : Bukkit.getOnlinePlayers()) {
            GamePlayer gamePlayer = Core.getPlayerManager().getPlayer(pl.getName());

            if (gamePlayer.getRank().getId() >= 3) {
                gamePlayer.message(MsgUtil.MsgType.INFO, "§f" + player.getName() + " §b" + (p.isAdminMode() ? "went into" : "went out of") +
                        " Admin Mode.");
            }
        }
    }

}