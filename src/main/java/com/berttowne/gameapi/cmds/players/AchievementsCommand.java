package com.berttowne.gameapi.cmds.players;

import com.berttowne.gameapi.Core;
import com.berttowne.gameapi.achievement.AchievementsGUI;
import com.berttowne.gameapi.cmd.Command;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class AchievementsCommand extends Command {

    public AchievementsCommand() {
        super(Core.getCommandHandler(), "achievements");
    }

    @Override
    public void execute(Player player, String... args) {
        AchievementsGUI gui = new AchievementsGUI(player, "all");
        gui.open(player);

        player.playSound(player.getLocation(), Sound.BLOCK_CHEST_OPEN, 1f, 1f);
    }

}