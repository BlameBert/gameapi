package com.berttowne.gameapi.cmds.currency;

import com.berttowne.gameapi.Core;
import com.berttowne.gameapi.cmd.Command;
import com.berttowne.gameapi.player.GamePlayer;
import com.berttowne.gameapi.util.MsgUtil;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class ShardsCommand extends Command {

    public ShardsCommand() {
        super(Core.getCommandHandler(), "shards");
    }

    @Override
    public void execute(Player player, String... args) {
        GamePlayer p = Core.getPlayerManager().createPlayer(player.getName());

        if (p.getRank().getId() < 3) {
            p.message(MsgUtil.MsgType.DANGER, "You do not have permission to use this command!");
            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BASS, 1f, 1f);
            return;
        }

        if (args.length != 3) {
            p.message(MsgUtil.MsgType.DANGER, "Invalid arguments! Use /shards <player> <add | remove | set> [amount]");
            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BASS, 1f, 1f);
            return;
        }

        if (Core.getPlayerManager().getPlayer(args[0]) != null) {
            GamePlayer target = Core.getPlayerManager().getPlayer(args[0]);

            if (args[1].equalsIgnoreCase("add")) {
                target.setShards(target.getShards() + Integer.parseInt(args[2]));
                target.message(MsgUtil.MsgType.SUCCESS, "§a§l" + args[2] + " shards §ahave been added to your account!");
                player.sendMessage(MsgUtil.MsgType.SUCCESS.getPrefix() + "Added §a§l" + args[2] + " shards §ato " + player.getName() + "'s" +
                        " account!");
                return;
            } else if (args[1].equalsIgnoreCase("remove")) {
                target.setShards(target.getShards() - Integer.parseInt(args[2]));
                target.message(MsgUtil.MsgType.DANGER, "§c§l" + args[2] + " shards §chave been removed from your account!");
                player.sendMessage(MsgUtil.MsgType.SUCCESS.getPrefix() + "Removed §a§l" + args[2] + " shards §afrom " + player.getName() + "'s" +
                        " account!");
                return;
            } else if (args[1].equalsIgnoreCase("set")) {
                target.setShards(Integer.parseInt(args[2]));
                target.message(MsgUtil.MsgType.WARNING, "Your new shard balance is §e§l" + args[2] + " shards§e!");
                player.sendMessage(MsgUtil.MsgType.SUCCESS.getPrefix() + "Set " + player.getName() + "'s" +
                        " shards to §a§l" + args[2] + "§a!");
            } else {
                player.sendMessage(MsgUtil.MsgType.DANGER.getPrefix() + "Invalid arguments! Use /shards <player> <add | remove | set> [amount]");
            }
        } else {
            p.message(MsgUtil.MsgType.DANGER, "Player §f" + args[0] + " §cnot found! Have they joined the server before?");
            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BASS, 1f, 1f);
        }
    }

}