package com.berttowne.gameapi.cmds.fetchers;

import com.berttowne.gameapi.Core;
import com.berttowne.gameapi.cmd.Command;
import com.berttowne.gameapi.player.GamePlayer;
import com.berttowne.gameapi.util.MsgUtil;
import com.berttowne.gameapi.util.fetchers.NameHistory;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class NamesCommand extends Command {

    public NamesCommand() {
        super(Core.getCommandHandler(), "names");
    }

    @Override
    public void execute(Player player, String... args) {
        GamePlayer p = Core.getPlayerManager().createPlayer(player.getName());

        if (p.getRank().getId() < 2) {
            p.message(MsgUtil.MsgType.DANGER, "You do not have permission to use this command!");
            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BASS, 1f, 1f);
            return;
        }

        if (args.length != 1) {
            p.message(MsgUtil.MsgType.DANGER, "Invalid arguments! Use /names <Player>");
            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BASS, 1f, 1f);
            return;
        }

        NameHistory.printPreviousNames(player, args[0]);
    }

}