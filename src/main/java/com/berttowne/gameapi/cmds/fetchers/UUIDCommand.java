package com.berttowne.gameapi.cmds.fetchers;

import com.berttowne.gameapi.Core;
import com.berttowne.gameapi.cmd.Command;
import com.berttowne.gameapi.util.MsgUtil;
import com.berttowne.gameapi.util.fetchers.UUIDFetcher;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class UUIDCommand extends Command {

    public UUIDCommand() {
        super(Core.getCommandHandler(), "uuid");
    }

    @Override
    public void execute(Player player, String... args) {
        if (Core.getPlayerManager().getRank(player.getName()).getId() < 2) {
            player.sendMessage(MsgUtil.MsgType.DANGER.getPrefix() + "You do not have permission to use this command!");
            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BASS, 1f, 1f);
            return;
        }

        if (args.length != 1) {
            player.sendMessage(MsgUtil.MsgType.DANGER.getPrefix() + "Invalid arguments! Use /uuid <Player>");
            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BASS, 1f, 1f);
            return;
        }

        final String input = args[0];
        final Player pl = Bukkit.getPlayer(input);

        String uuid;
        String pl1;

        if (pl != null && Bukkit.getServer().getOnlineMode()) {
            uuid = pl.getUniqueId().toString();
            pl1 = pl.getName();
        } else {
            pl1 = input;

            String id = UUIDFetcher.getUUIDOf(input);
            if (!id.equals("")) {
                uuid = id;
            } else {
                player.sendMessage(MsgUtil.MsgType.DANGER.getPrefix() + "Player §f" + input + " §cdoes not exist");
                return;
            }
        }

        player.sendMessage(MsgUtil.MsgType.SUCCESS.getPrefix() + "§f" + pl1 + "'s §aUUID is §f" + uuid);
    }

}