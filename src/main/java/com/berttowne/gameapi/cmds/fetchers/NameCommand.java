package com.berttowne.gameapi.cmds.fetchers;

import com.berttowne.gameapi.Core;
import com.berttowne.gameapi.cmd.Command;
import com.berttowne.gameapi.player.GamePlayer;
import com.berttowne.gameapi.util.MsgUtil;
import com.berttowne.gameapi.util.fetchers.NameFetcher;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import java.util.UUID;

public class NameCommand extends Command {

    public NameCommand() {
        super(Core.getCommandHandler(), "name");
    }

    @Override
    public void execute(Player player, String... args) {
        GamePlayer p = Core.getPlayerManager().createPlayer(player.getName());

        if (p.getRank().getId() < 2) {
            p.message(MsgUtil.MsgType.DANGER, "You do not have permission to use this command!");
            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BASS, 1f, 1f);
            return;
        }

        if (args.length != 1) {
            p.message(MsgUtil.MsgType.DANGER, "Invalid arguments! Use /name <UUID>");
            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BASS, 1f, 1f);
            return;
        }

        if (args[0].matches("[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}")) {
            String name = NameFetcher.getNameForUUID(UUID.fromString(args[0]));

            if (name != null && !name.equals("")) {
                p.message(MsgUtil.MsgType.SUCCESS, "§2§lUUID: §a" + args[0]);
                p.message(MsgUtil.MsgType.SUCCESS, "§2§lNAME: §a" + name);
            }
        } else {
            p.message(MsgUtil.MsgType.DANGER, "Invalid UUID!");
        }
    }

}