package com.berttowne.gameapi.friends;

import com.berttowne.gameapi.Core;
import com.berttowne.gameapi.util.MsgUtil;
import com.berttowne.gameapi.util.fetchers.NameFetcher;
import com.berttowne.gameapi.util.fetchers.UUIDFetcher;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.UUID;

public class FriendManager {

    public ArrayList<FriendRequest> getSentRequests(String player) {
        ArrayList<FriendRequest> friendRequests = new ArrayList<>();

        new BukkitRunnable() {
            @Override
            public void run() {
                String uuid;
                if (Bukkit.getPlayer(player).isOnline()) {
                    uuid = Bukkit.getPlayer(player).getUniqueId().toString();
                } else {
                    uuid = UUIDFetcher.getUUIDOf(player);
                }

                Connection con = Core.getConnection();
                String sql = "SELECT * FROM friends WHERE uuid_p1 = ?";

                try {
                    PreparedStatement stmt = con.prepareStatement(sql);
                    stmt.setString(1, uuid);
                    ResultSet set = stmt.executeQuery();

                    while (set.next()) {
                        if (Bukkit.getPlayer(UUID.fromString(set.getString("uuid_p2"))).isOnline()) {
                            FriendRequest friendRequest = new FriendRequest(Bukkit.getPlayer(UUID.fromString(set.getString("uuid_p2"))).getName()
                                    , uuid);
                            friendRequests.add(friendRequest);
                        } else {
                            FriendRequest friendRequest = new FriendRequest(NameFetcher.getNameForUUID(UUID.fromString(set.getString("uuid_p2")))
                                    , uuid);
                            friendRequests.add(friendRequest);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Core.getPlugin());

        return friendRequests;
    }

    public ArrayList<FriendRequest> getPendingRequests(String player) {
        ArrayList<FriendRequest> friendRequests = new ArrayList<>();

        new BukkitRunnable() {
            @Override
            public void run() {
                String uuid;
                if (Bukkit.getPlayer(player).isOnline()) {
                    uuid = Bukkit.getPlayer(player).getUniqueId().toString();
                } else {
                    uuid = UUIDFetcher.getUUIDOf(player);
                }

                Connection con = Core.getConnection();
                String sql = "SELECT * FROM friends WHERE uuid_p2 = ?";

                try {
                    PreparedStatement stmt = con.prepareStatement(sql);
                    stmt.setString(1, uuid);
                    ResultSet set = stmt.executeQuery();

                    while (set.next()) {
                        if (Bukkit.getPlayer(UUID.fromString(set.getString("uuid_p1"))).isOnline()) {
                            FriendRequest friendRequest = new FriendRequest(Bukkit.getPlayer(UUID.fromString(set.getString("uuid_p1"))).getName()
                                    , uuid);
                            friendRequests.add(friendRequest);
                        } else {
                            FriendRequest friendRequest = new FriendRequest(NameFetcher.getNameForUUID(UUID.fromString(set.getString("uuid_p1")))
                                    , uuid);
                            friendRequests.add(friendRequest);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Core.getPlugin());

        return friendRequests;
    }

    public boolean alreadySent(String to, String from) {
        for (FriendRequest friendRequest : getSentRequests(from)) {
            if (friendRequest.getTo().equalsIgnoreCase(to)) {
                return true;
            }
        }
        return false;
    }

    public boolean alreadyRecieved(String to, String from) {
        for (FriendRequest friendRequest : getPendingRequests(to)) {
            if (friendRequest.getFrom().equalsIgnoreCase(from)) {
                return true;
            }
        }

        return false;
    }

    public void sendFriendRequest(String to, String from) {
        if (!alreadySent(to, from)) {
            OfflinePlayer pto = Bukkit.getOfflinePlayer(to);
            Player pfrom = Bukkit.getPlayer(from);

            if (pto.hasPlayedBefore()) {
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        Connection con = Core.getConnection();
                        String sql = "INSERT INTO `friends`(uuid_p1, uuid_p2, accepted, deleted) VALUES (?, ?, ?, ?)"; // 1 = from, 2 = to

                        try {
                            PreparedStatement stmt = con.prepareStatement(sql);
                            stmt.setString(1, pfrom.getUniqueId().toString());
                            stmt.setString(2, pto.getUniqueId().toString());
                            stmt.setBoolean(3, false);
                            stmt.setBoolean(4, false);
                            stmt.executeUpdate();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }.runTaskAsynchronously(Core.getPlugin());

                if (pto.isOnline()) {
                    Player p = (Player) pto;

                    p.sendMessage(MsgUtil.MsgType.INFO.getPrefix() + "Incoming friend request from §f" + from);
                    p.spigot().sendMessage(
                            new ComponentBuilder("[ACCEPT]")
                            .color(ChatColor.GREEN)
                            .bold(true)
                            .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Click to accept request from " + from)
                                    .create()))
                            .event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "friend accept " + from))

                            .append(" - ")
                            .color(ChatColor.DARK_GRAY)
                            .bold(true)

                            .append("[DENY]")
                            .color(ChatColor.RED)
                            .bold(true)
                            .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Click to deny request from " + from)
                                    .create()))
                            .event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "friend deny " + from))
                            .create()
                    );
                }
            } else {
                pfrom.sendMessage(MsgUtil.MsgType.DANGER.getPrefix() + "Player " + to + " can't be found. Have they joined the server before?");
                pfrom.playSound(pfrom.getLocation(), Sound.BLOCK_NOTE_BASS, 1f, 1f);
            }
        }
    }

    // TODO: Add ability to accept, deny, and hide friend REQUESTS
    public void acceptRequest(String to, String from) {

    }

    // TODO: Add ability to get a list of friends and their info and remove friends

    // TODO: Add commands and a GUI  to handle friends

}