package com.berttowne.gameapi.friends;

public class FriendRequest {

    private String from;
    private String to;

    public FriendRequest(String from, String to) {
        this.from = from;
        this.to = to;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

}