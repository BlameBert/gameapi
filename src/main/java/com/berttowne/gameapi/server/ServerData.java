package com.berttowne.gameapi.server;

import com.berttowne.gameapi.player.Rank;

public class ServerData {

    private String id;
    private String ip;

    private int players;
    private int maxPlayers;

    private Rank minRank;
    private ServerState state;

    public ServerData(String id, String ip, int players, int maxPlayers, Rank minRank, ServerState state) {
        this.id = id;
        this.ip = ip;
        this.players = players;
        this.maxPlayers = maxPlayers;
        this.minRank = minRank;
        this.state = state;
    }

    public String getId() {
        return id;
    }

    public String getIp() {
        return ip;
    }

    public int getPlayers() {
        return players;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public Rank getMinRank() {
        return minRank;
    }

    public void setMinRank(Rank minRank) {
        this.minRank = minRank;
    }

    public ServerState getState() {
        return state;
    }

    public void setState(ServerState state) {
        this.state = state;
    }

}