package com.berttowne.gameapi.server;

import org.bukkit.Material;

public class ServerState {

    private String name;
    private String displayName;
    private boolean joinable;
    private Material display;

    public ServerState(String name, String displayName, boolean joinable, Material display) {
        this.name = name;
        this.displayName = displayName;
        this.joinable = joinable;
        this.display = display;
    }

    public String getName() {
        return name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public boolean isJoinable() {
        return joinable;
    }

    public Material getDisplay() {
        return display;
    }

}