package com.berttowne.gameapi.server;

import com.berttowne.gameapi.Core;
import com.berttowne.gameapi.gui.GUI;
import com.berttowne.gameapi.util.GlowUtil;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class ServerGUI extends GUI {

    public ServerGUI() {
        super("Server List", 54);

        this.setUpdateTicks(20);
    }

    @Override
    protected void onPlayerClick(InventoryClickEvent event) {
        event.setCancelled(true);
    }

    @Override
    protected void populate() {
        for (int i = 0; i < inventory.getSize(); i++) {
            if (i < 9 || i > 44 || (i + 1) % 9 == 0 || i % 9 == 0) {
                inventory.setItem(i, SPACER);
            }
        }

        {
            ItemStack item = new ItemStack(Material.BOOK);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§e§lServer List");
            item.setItemMeta(meta);
            inventory.setItem(4, item);
        }

        for (ServerData server : Core.getServerCache().values()) {
            if (!server.getState().getName().equalsIgnoreCase("Offline")) {
                if (server.getPlayers() == server.getMaxPlayers()) {
                    ItemStack item = new ItemStack(Material.REDSTONE_BLOCK);
                    ItemMeta meta = item.getItemMeta();
                    meta.setDisplayName("§4§lFULL §8- §c§l" + server.getId());
                    meta.setLore(Arrays.asList(
                            "§c",
                            "§fState: §7" + server.getState().getDisplayName(),
                            "§fRank: §7" + server.getMinRank().getPrefix(),
                            "§fPlayers: §7" + server.getPlayers() + "§8/§7" + server.getMaxPlayers(),
                            "§fJoinable: §cNo"
                    ));
                    item.setItemMeta(meta);

                    if (server.getIp().equalsIgnoreCase(Bukkit.getServer().getIp() + ":" + Bukkit.getServer().getPort())) {
                        GlowUtil.addGlow(item);
                    }

                    inventory.addItem(item);
                } else {
                    ItemStack item = new ItemStack(server.getState().getDisplay());
                    ItemMeta meta = item.getItemMeta();
                    meta.setDisplayName("§a§l" + server.getId());
                    meta.setLore(Arrays.asList(
                            "§c",
                            "§fState: §7" + server.getState().getDisplayName(),
                            "§fRank: §7" + server.getMinRank().getPrefix(),
                            "§fPlayers: §7" + server.getPlayers() + "§8/§7" + server.getMaxPlayers(),
                            "§fJoinable: " + (server.getState().isJoinable() ? "§aYes" : "§cNo")
                    ));
                    item.setItemMeta(meta);

                    if (server.getIp().equalsIgnoreCase(Bukkit.getServer().getIp() + ":" + Bukkit.getServer().getPort())) {
                        GlowUtil.addGlow(item);
                    }

                    inventory.addItem(item);
                }
            } else {
                ItemStack item = new ItemStack(server.getState().getDisplay());
                ItemMeta meta = item.getItemMeta();
                meta.setDisplayName("§4§lOFFLINE §8- §c§l" + server.getId());
                meta.setLore(Arrays.asList(
                        "§c",
                        "§7§oThis server is currently offline.",
                        "§7§oPlease try again later!"
                ));
                item.setItemMeta(meta);

                inventory.addItem(item);
            }
        }
    }

}