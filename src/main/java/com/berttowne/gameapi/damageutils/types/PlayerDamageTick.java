package com.berttowne.gameapi.damageutils.types;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.text.DecimalFormat;

public class PlayerDamageTick extends MonsterDamageTick {

    public PlayerDamageTick(double damage, String name, long timestamp, Player player) {
        super(damage, name, timestamp, player);
    }

    public PlayerDamageTick(double damage, String name, long timestamp, Player player, double distance) {
        super(damage, name, timestamp, player, distance);
    }

    public Player getPlayer() {
        return Bukkit.getPlayer(this.getEntityUUID());
    }

    @Override
    public String getDeathMessage(Player player) {
        DecimalFormat df = new DecimalFormat("#.#");
        return getDeathMessageTemplate(player).replace("{KILLER}", getPlayer().getDisplayName() + "§8§l(§7" + df.format(getPlayer().getHealth())
                        + "❤§8§l)");
    }

    @Override
    public String getSingleLineSummary() {
        DecimalFormat df = new DecimalFormat("#.#");
        return getMessageTemplate().replace("{ATTACKER}", getPlayer().getDisplayName() + "§8§l(§7" + df.format(getPlayer().getHealth())
                + "❤§8§l)");
    }

}