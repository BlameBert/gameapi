package com.berttowne.gameapi.damageutils.types;

import com.berttowne.gameapi.damageutils.DamageTick;
import com.berttowne.gameapi.util.LocationUtil;
import com.berttowne.gameapi.util.MsgUtil;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;

public class BlockDamageTick extends DamageTick {

    private Material type;
    private Location location;

    public BlockDamageTick(double damage, EntityDamageEvent.DamageCause cause, String name, long timestamp, Material type, Location location) {
        super(damage, cause, name, timestamp);
        this.type = type;
        this.location = location;
    }

    public Material getType() {
        return type;
    }

    public Location getLocation() {
        return location;
    }

    @Override
    public boolean matches(DamageTick tick) {
        return (tick instanceof BlockDamageTick) && ((BlockDamageTick) tick).getType().equals(getType()) && LocationUtil.samePlace(getLocation(), ((BlockDamageTick) tick).getLocation());
    }

    @Override
    public String getDeathMessage(Player player) {
        return MsgUtil.MsgType.DANGER.getPrefix() + "§f" + player.getDisplayName() +
                "§c was killed by §f" + getType().name().replace("_", " ");
    }

    @Override
    public String getSingleLineSummary() {
        return "§cHurt by §f" + getType().name().replace("_", " ");
    }

}