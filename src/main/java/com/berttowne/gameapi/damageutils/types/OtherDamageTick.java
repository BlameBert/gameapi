package com.berttowne.gameapi.damageutils.types;

import com.berttowne.gameapi.damageutils.DamageTick;
import com.berttowne.gameapi.util.MsgUtil;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;

public class OtherDamageTick extends DamageTick {

    public OtherDamageTick(double damage, EntityDamageEvent.DamageCause cause, String name, long timestamp) {
        super(damage, cause, name, timestamp);
    }

    @Override
    public boolean matches(DamageTick tick) {
        return (tick instanceof OtherDamageTick) && tick.getEntityName().equals(getEntityName());
    }

    @Override
    public String getDeathMessage(Player player) {
        return MsgUtil.MsgType.DANGER.getPrefix() + "§f" + player.getDisplayName() + " §cwas killed by §f" + getEntityName();
    }

    @Override
    public String getSingleLineSummary() {
        return "§f" + getEntityName() + " §cdamage";
    }

}