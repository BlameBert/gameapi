package com.berttowne.gameapi.damageutils.types;

import com.berttowne.gameapi.damageutils.DamageTick;
import com.berttowne.gameapi.util.LocationUtil;
import com.berttowne.gameapi.util.MsgUtil;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class TNTDamageTick extends PlayerDamageTick {

    private Location location;

    public TNTDamageTick(double damage, String name, long timestamp, Player player, Location location) {
        super(damage, name, timestamp, player);
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }

    @Override
    public boolean matches(DamageTick tick) {
        return (tick instanceof TNTDamageTick) && LocationUtil.samePlace(getLocation(), ((TNTDamageTick) tick).getLocation()) && getPlayer().getUniqueId().equals(((TNTDamageTick) tick).getPlayer().getUniqueId());
    }

    @Override
    public String getDeathMessage(Player player) {
        return MsgUtil.MsgType.DANGER.getPrefix() + "§f" + player.getDisplayName() + " §cwas killed by §f" + getPlayer().getDisplayName() + "'s TNT";
    }

    @Override
    public String getSingleLineSummary() {
        return "§cHurt by §f" + getPlayer().getDisplayName() + "'s TNT";
    }

}