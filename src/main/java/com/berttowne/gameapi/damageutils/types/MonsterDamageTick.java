package com.berttowne.gameapi.damageutils.types;

import com.berttowne.gameapi.damageutils.DamageTick;
import com.berttowne.gameapi.util.EntityUtil;
import com.berttowne.gameapi.util.MsgUtil;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;

import java.text.DecimalFormat;
import java.util.UUID;

public class MonsterDamageTick extends DamageTick {

    private UUID entityUUID;
    private String entityName;
    private double distance;
    private boolean ranged = false;

    public MonsterDamageTick(double damage, String name, long timestamp, LivingEntity entity) {
        super(damage, EntityDamageEvent.DamageCause.ENTITY_ATTACK, name, timestamp);
        this.entityUUID = entity.getUniqueId();
        this.entityName = EntityUtil.getEntityName(entity);
    }

    public MonsterDamageTick(double damage, String name, long timestamp, LivingEntity entity, double distance) {
        super(damage, EntityDamageEvent.DamageCause.ENTITY_ATTACK, name, timestamp);
        this.entityUUID = entity.getUniqueId();
        this.entityName = EntityUtil.getEntityName(entity);
        this.distance = distance;
        this.ranged = true;
    }

    public double getDistance() {
        return distance;
    }

    public boolean isRanged() {
        return ranged;
    }

    protected String getMessageTemplate() {
        if (isRanged()) {
            DecimalFormat df = new DecimalFormat("#.#");
            return "§cShot by §f{ATTACKER} §cfrom §f" + df.format(getDistance()) + "m away";
        } else {
            return "§cAttacked by §f{ATTACKER}";

        }
    }

    public UUID getEntityUUID() {
        return entityUUID;
    }

    public String getEntityName() {
        return entityName;
    }

    protected String getDeathMessageTemplate(Player player) {
        if (isRanged()) {
            DecimalFormat df = new DecimalFormat("#.#");
            return "§f" + player.getDisplayName() + " §cwas killed by §f{KILLER} §cfrom §f" + df.format(getDistance()) + "m away";
        } else {
            return "§f" + player.getDisplayName() + " §cwas killed by §f{KILLER}";
        }
    }

    @Override
    public boolean matches(DamageTick tick) {
        return (tick instanceof MonsterDamageTick) && this.entityUUID.equals(((MonsterDamageTick) tick).getEntityUUID());
    }

    @Override
    public String getDeathMessage(Player player) {
        return MsgUtil.MsgType.DANGER.getPrefix() + getDeathMessageTemplate(player).replace("{KILLER}", this.entityName);

    }

    @Override
    public String getSingleLineSummary() {
        return getMessageTemplate().replace("{ATTACKER}", this.entityName);
    }

}