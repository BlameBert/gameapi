package com.berttowne.gameapi.damageutils.types;

import com.berttowne.gameapi.damageutils.DamageTick;
import com.berttowne.gameapi.util.MsgUtil;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;

import java.text.DecimalFormat;

public class FallDamageTick extends DamageTick {

    private double distance;

    public FallDamageTick(double damage, String name, long timestamp, double distance) {
        super(damage, EntityDamageEvent.DamageCause.FALL, name, timestamp);
        this.distance = distance;
    }

    public double getDistance() {
        return distance;
    }

    @Override
    public boolean matches(DamageTick tick) {
        return false;
    }

    @Override
    public String getDeathMessage(Player player) {
        DecimalFormat df = new DecimalFormat("#");
        return MsgUtil.MsgType.DANGER.getPrefix() + "§f" + player.getDisplayName() +
                " §cwas killed by §f" + df.format(getDistance()) + " block fall";
    }

    @Override
    public String getSingleLineSummary() {
        DecimalFormat df = new DecimalFormat("#");
        return "§cFell §f" + df.format(getDistance()) + " blocks";
    }

}