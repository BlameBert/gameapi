package com.berttowne.gameapi.damageutils.listeners;

import com.berttowne.gameapi.damageutils.DamageManager;
import com.berttowne.gameapi.damageutils.DamageTick;
import com.berttowne.gameapi.damageutils.KillAssist;
import com.berttowne.gameapi.damageutils.events.PlayerAssistedWithDeathEvent;
import com.berttowne.gameapi.util.MsgUtil;
import org.bukkit.Bukkit;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;

import java.util.ArrayList;
import java.util.List;

public class DeathListener implements Listener {

    @EventHandler
    public void onDeath(EntityDeathEvent event) {
        LivingEntity entity = event.getEntity();

        if ((event instanceof PlayerDeathEvent)) {
            ((PlayerDeathEvent) event).setDeathMessage(null);

            Player player = (Player) entity;

            List<DamageTick> ticks = DamageManager.getLoggedTicks(player.getUniqueId());
            List<String> summary = DamageManager.getDamageSummary(ticks);

            if (summary.size() > 0) {
                player.sendMessage("§4§l╔════════════════════════════");
                player.sendMessage("§4§l║");
                player.sendMessage("§4§l║   §c§lHOW YOU DIED");
                player.sendMessage("§4§l║");

                for (String msg : summary) {
                    player.sendMessage("§4§l║   " + msg);
                }

                player.sendMessage("§4§l║");
                player.sendMessage("§4§l╚════════════════════════════");

                int more = ticks.size() - 1;

                List<KillAssist> assists = DamageManager.getPossibleAssists(ticks);

                ((PlayerDeathEvent) event).setDeathMessage(ticks.get(ticks.size() - 1).getDeathMessage(player));

                if (assists.size() > 0) {
                    String assistText = "§c, assisted by ";

                    List<String> names = new ArrayList<>();

                    int morePlayers = 0;

                    for (KillAssist assist : assists) {
                        PlayerAssistedWithDeathEvent e = new PlayerAssistedWithDeathEvent(assist, player);
                        Bukkit.getPluginManager().callEvent(e);

                        if (e.isCancelled()) {
                            continue;
                        }

                        if (names.size() >= 3) {
                            morePlayers++;
                            continue;
                        }

                        names.add("§f" + assist.getAttacker().getDisplayName() + "§c");
                    }

                    assistText += names.toString().replace("[", "").replace("]", "");

                    if (morePlayers > 0) {
                        assistText += " + " + morePlayers + " other player" + (morePlayers != 1 ? "s" : "");
                    }

                    ((PlayerDeathEvent) event).setDeathMessage(MsgUtil.MsgType.DANGER.getPrefix() +
                            ((PlayerDeathEvent) event).getDeathMessage() + assistText);
                } else if (more > 0) {
                    ((PlayerDeathEvent) event).setDeathMessage(MsgUtil.MsgType.DANGER.getPrefix() +
                            ((PlayerDeathEvent) event).getDeathMessage() + " §c+ " + more + " more");
                }

            }

        }

        DamageManager.dump(entity.getUniqueId());
    }

}