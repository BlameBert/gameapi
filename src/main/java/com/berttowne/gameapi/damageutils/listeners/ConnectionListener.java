package com.berttowne.gameapi.damageutils.listeners;

import com.berttowne.gameapi.damageutils.DamageManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class ConnectionListener implements Listener {

    @EventHandler
    public void onLogout(PlayerQuitEvent event) {
        DamageManager.dump(event.getPlayer().getUniqueId());
    }

}