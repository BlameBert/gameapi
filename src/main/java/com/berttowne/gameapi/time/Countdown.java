package com.berttowne.gameapi.time;

/*
 * Created by BlameBert.
 * 
 * Date: 3/13/2017
 * Time: 5:45 PM
 *
 */

import com.berttowne.gameapi.Core;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

public abstract class Countdown {

    private int currentSeconds;
    private int currentMinutes;
    private int currentHours;

    private int maxSeconds;
    private int maxMinutes;
    private int maxHours;

    private int totalTicks;

    private BukkitTask timerTask;

    private boolean paused;

    public Countdown(int maxSeconds, int maxMinutes, int maxHours) {
        this.maxSeconds = maxSeconds;
        this.maxMinutes = maxMinutes;
        this.maxHours = maxHours;

        this.totalTicks = (maxHours * 60 * 60 * 20) + (maxMinutes * 60 * 20) + (maxSeconds * 20);

        this.currentSeconds = 0;
        this.currentMinutes = 0;
        this.currentHours = 0;

        this.timerTask = null;
        this.paused = true;
    }

    public void start() {
        this.paused = false;
        onStart();

        this.timerTask = Bukkit.getScheduler().runTaskTimer(Core.getPlugin(), new BukkitRunnable() {
            @Override
            public void run() {
                if (!paused) {
                    currentSeconds--;
                    if (currentSeconds == -1) {
                        currentSeconds = 60;
                        currentMinutes--;
                        if (currentMinutes == -1) {
                            currentMinutes = 60;
                            currentHours--;
                        }
                    }

                    if (currentHours == maxHours && currentMinutes == maxMinutes && currentSeconds == maxSeconds) {
                        onStop();
                        setPaused(true);

                        this.cancel();
                    }

                    onTime(currentHours, currentMinutes, currentSeconds);
                }
            }
        }, 0L, 20L);
    }

    public void setPaused(boolean paused) {
        this.paused = paused;
    }

    protected abstract void onStart();

    protected abstract void onStop();

    protected abstract void onTime(int hours, int minutes, int seconds);

    public String getFormattedTimeClock(ChatColor mainColor, ChatColor accentColor) {
        return mainColor + (currentHours < 10 ? "0" : "") + currentHours + accentColor + ":"
                + mainColor + (currentMinutes < 10 ? "0" : "") + currentMinutes + accentColor + ":"
                + mainColor + (currentSeconds < 10 ? "0" : "") + currentSeconds;
    }

    public String getFormattedTimeWords(ChatColor mainColor, ChatColor accentColor, boolean fullWord) {
        return "" + mainColor + currentHours + accentColor + (fullWord ? (currentHours != 1 ? " hours" : " hour") : "h") +
                mainColor + currentMinutes + accentColor + (fullWord ? (currentMinutes != 1 ? " minutes" : " minute") : "m") +
                mainColor + currentSeconds + accentColor + (fullWord ? (currentSeconds != 1 ? " seconds" : " second") : "s");
    }

}