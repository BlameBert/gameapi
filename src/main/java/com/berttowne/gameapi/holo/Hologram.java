package com.berttowne.gameapi.holo;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import java.util.*;

public interface Hologram {

    public void register(Location loc, String... lines);

    public void register(Location loc, List<String> lines);

    public boolean display(Player p);

    public boolean destroy(Player p);

    /*private static ArrayList<Hologram> holograms = new ArrayList<>();

    private String[] lines;
    private Location location;
    private ArrayList<ArmorStand> armorStands;

    public static ArrayList<Hologram> getHolograms() {
        return holograms;
    }

    public static void despawnAll() {
        for (Hologram hologram : getHolograms()) {
            hologram.remove();
        }
    }

    public Hologram(Location location, String... lines) {
        this.location = location;
        this.lines = lines;
        this.armorStands = new ArrayList<>();

        ArrayList<String> text = new ArrayList<>(Arrays.asList(lines));

        Collections.reverse(text);

        for (String line : text) {
            ArmorStand stand = location.getWorld().spawn(location, ArmorStand.class);

            stand.setVisible(false);
            stand.setCustomName(line);
            stand.setCustomNameVisible(true);
            stand.setSmall(false);
            stand.setBasePlate(false);
            stand.setGravity(false);
            stand.setInvulnerable(true);

            armorStands.add(stand);

            this.location.add(0.0, 0.3, 0.0);
        }

        holograms.add(this);
    }

    public Location getLocation() {
        return location;
    }

    public ArrayList<ArmorStand> getArmorStands() {
        return armorStands;
    }

    public void remove() {
        for (ArmorStand armorStand : armorStands) {
            armorStand.remove();
        }

        holograms.remove(this);
    }*/

}