package com.berttowne.gameapi.holo;

import com.berttowne.gameapi.holo.impl.Wrapper1_10_R1;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import java.util.Arrays;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public enum Version {

    ONE_TEN("1_10_R1", Wrapper1_10_R1.class);

    private static final LoadingCache<Class<? extends Hologram>, Hologram> WRAPPER_CACHE =
            CacheBuilder.newBuilder()
                    .maximumSize(values().length)
                    .expireAfterWrite(5, TimeUnit.MINUTES)
                    .build(new CacheLoader<Class<? extends Hologram>, Hologram>() {
                        @Override
                        public Hologram load(Class<? extends Hologram> aClass) throws Exception {
                            return aClass.newInstance();
                        }
                    });


    private final String pkg;

    private final Class<? extends Hologram> wrapper;

    Version(String pkg, Class<? extends Hologram> wrapper) {
        this.pkg = pkg;
        this.wrapper = wrapper;
    }

    public String getPkg() {
        return pkg;
    }

    public Hologram getWrapper() {
        try {
            return WRAPPER_CACHE.get(wrapper);
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    public static Version of(final String pkg) {
        return Arrays.stream(values()).filter(ver -> pkg.equals("v" + ver.getPkg())).findFirst().orElse(null);
    }

}