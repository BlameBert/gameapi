package com.berttowne.gameapi.holo.impl;

import com.berttowne.gameapi.holo.Hologram;
import net.minecraft.server.v1_10_R1.*;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_10_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Wrapper1_10_R1 implements Hologram {

    private List<String> lines;
    private Location loc;
    private static final double ABS = 0.23D;

    private ArrayList<EntityArmorStand> armorStands;

    @Override
    public void register(Location loc, String... lines) {
        this.lines = Arrays.asList(lines);
        this.loc = loc;

        this.armorStands = new ArrayList<>();
    }

    @Override
    public void register(Location loc, List<String> lines) {
        this.lines = lines;
        this.loc = loc;

        this.armorStands = new ArrayList<>();
    }

    @Override
    public boolean display(Player player) {
        Location displayLoc = loc.clone().add(0, (ABS * lines.size()) - 1.97D, 0);

        PlayerConnection connection = ((CraftPlayer) player).getHandle().playerConnection;

        WorldServer nmsWorld = ((CraftWorld) displayLoc.getWorld()).getHandle();

        for (String line : lines) {
            EntityArmorStand armorStand = new EntityArmorStand(nmsWorld, displayLoc.getX(), displayLoc.getY(), displayLoc.getZ());

            armorStand.setInvisible(true);
            armorStand.setCustomName(line);
            armorStand.setCustomNameVisible(true);
            armorStand.setInvulnerable(true);
            armorStand.setBasePlate(false);
            armorStand.setArms(false);
            armorStand.setNoGravity(true);
            armorStand.setLocation(displayLoc.getX(), displayLoc.getY(), displayLoc.getZ(), 0.0F, 0.0F);

            PacketPlayOutSpawnEntityLiving packetPlayOutSpawnEntity = new PacketPlayOutSpawnEntityLiving(armorStand);
            connection.sendPacket(packetPlayOutSpawnEntity);

            armorStands.add(armorStand);

            displayLoc.add(0, -ABS, 0);
        }

        return true;
    }

    @Override
    public boolean destroy(Player player) {
        PlayerConnection connection = ((CraftPlayer) player).getHandle().playerConnection;
        for (EntityArmorStand armorStand : armorStands) {
            connection.sendPacket(new PacketPlayOutEntityDestroy(armorStand.getId()));
        }

        return true;
    }

}