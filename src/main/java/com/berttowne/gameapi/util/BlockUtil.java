package com.berttowne.gameapi.util;

/*
 * Created by BlameBert.
 * 
 * Date: 3/11/2017
 * Time: 9:07 PM
 *
 */

import com.berttowne.gameapi.Core;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.block.BlockState;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;

public class BlockUtil {

    public static void regen(List<BlockState> blocks, boolean effect, int speed) {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(Core.getPlugin(), new BukkitRunnable() {
            int i = -1;

            @SuppressWarnings("deprecation")
            public void run() {
                if (i != blocks.size() - 1) {
                    i++;

                    BlockState bs = blocks.get(i);
                    bs.getBlock().setType(bs.getType());
                    bs.getBlock().setData(bs.getData().getData());

                    if (effect) {
                        bs.getBlock().getWorld().playEffect(bs.getLocation(), Effect.STEP_SOUND, bs.getBlock().getType());
                        bs.getBlock().getWorld().playEffect(bs.getLocation(), Effect.STEP_SOUND, bs.getBlock().getData());
                    }
                } else {
                    this.cancel();
                }
            }
        }, speed * 20, speed * 20);
    }

}