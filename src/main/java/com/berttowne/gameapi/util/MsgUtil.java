package com.berttowne.gameapi.util;

public class MsgUtil {

    public static enum MsgType {

        DEFAULT("§8§l[!] §7"),
        PRIMARY("§1§l[!] §9"),
        SUCCESS("§2§l[!] §a"),
        INFO("§3§l[!] §b"),
        WARNING("§6§l[!] §e"),
        DANGER("§4§l[!] §c");

        private String prefix;

        MsgType(String prefix) {
            this.prefix = prefix;
        }

        public String getPrefix() {
            return prefix;
        }

    }

    public static String calculateTime(int ticks, boolean showHours) {
        int hours = ticks / 3600;
        int r = ticks % 3600;
        int mins = r / 60;
        int secs = r % 60;

        if (showHours && hours > 0) {
            return hours + "h" + mins + "m" + secs + "s";
        } else if (mins > 0) {
            return mins + "m" + secs + "s";
        } else {
            return secs + "s";
        }
    }

}