package com.berttowne.gameapi.util;

import org.apache.commons.lang.WordUtils;
import org.bukkit.entity.Entity;

public class EntityUtil {

    public static String getEntityName(Entity entity) {
        if (entity.getCustomName() != null) {
            return entity.getCustomName();
        } else {
            String name = entity.getType().name();
            name = name.replace("_", " ");
            return WordUtils.capitalizeFully(name);
        }
    }

}