package com.berttowne.gameapi.util.fetchers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class UUIDFetcher {

    public static String getUUIDOf(String name) {
        try {
            URL url = new URL("https://api.mojang.com/users/profiles/minecraft/" + name + "?");

            BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
            String line = reader.readLine();

            String[] id = line.split(",");

            StringBuilder stringBuffer = new StringBuilder(id[0].substring(7, 39));
            stringBuffer.insert(8, "-");
            stringBuffer.insert(13, "-");
            stringBuffer.insert(18, "-");
            stringBuffer.insert(23, "-");

            return stringBuffer.toString();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return null;
    }

}