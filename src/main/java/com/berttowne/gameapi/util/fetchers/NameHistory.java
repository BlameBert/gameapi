package com.berttowne.gameapi.util.fetchers;

import com.berttowne.gameapi.util.MsgUtil;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class NameHistory {

    private static final String LOOKUP_URL = "https://api.mojang.com/user/profiles/%s/names";

    private static final Gson JSON_PARSER = new Gson();

    public static PreviousPlayerNameEntry[] getPlayerPreviousNames(UUID player) {
        return getPlayerPreviousNames(player.toString());
    }

    public static PreviousPlayerNameEntry[] getPlayerPreviousNames(OfflinePlayer player) {
        return getPlayerPreviousNames(player.getUniqueId());
    }

    private static PreviousPlayerNameEntry[] getPlayerPreviousNames(String uuid) {
        try {
            if (uuid == null || uuid.isEmpty())
                return null;
            String response = getRawJsonResponse(new URL(String.format(LOOKUP_URL, uuid)));
            PreviousPlayerNameEntry[] names = JSON_PARSER.fromJson(response, PreviousPlayerNameEntry[].class);
            return names;
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return null;
    }

    private static String getRawJsonResponse(URL u) {
        try {
            HttpURLConnection con = (HttpURLConnection) u.openConnection();
            con.setDoInput(true);
            con.setConnectTimeout(2000);
            con.setReadTimeout(2000);
            con.connect();
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String response = in.readLine();
            in.close();
            return response;
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public static void printPreviousNames(Player toSend, String name) {

        PreviousPlayerNameEntry[] previousNames = getPlayerPreviousNames(UUIDFetcher.getUUIDOf(name));

        if (previousNames != null) {
            toSend.sendMessage(MsgUtil.MsgType.SUCCESS.getPrefix() + "§f" + name + " §ahas had §f" + previousNames.length + " §aname"
                    + (previousNames.length != 1 ? "s" : "") + ":");

            if (previousNames.length > 1) {
                for (PreviousPlayerNameEntry entry : previousNames) {
                    toSend.sendMessage(entry.toString());
                }
            } else {
                toSend.sendMessage(MsgUtil.MsgType.DANGER.getPrefix() + "§7- §cNo name changes to display");
            }
        } else {
            toSend.sendMessage(MsgUtil.MsgType.DANGER.getPrefix() + "That player doesn't exist!");
        }
    }

    public class PreviousPlayerNameEntry {

        private String name;

        @SerializedName("changedToAt")
        private long changeTime;

        public String getPlayerName() {
            return name;
        }

        public long getChangeTime() {
            return changeTime;
        }

        public boolean isPlayersInitialName() {
            return getChangeTime() == 0;
        }

        @Override
        public String toString() {
            SimpleDateFormat format = new SimpleDateFormat("MM/dd/yy");

            if (isPlayersInitialName()) {
                return MsgUtil.MsgType.SUCCESS.getPrefix() + "§7- §2§lOriginal Name: §f" + getPlayerName();
            }

            return MsgUtil.MsgType.SUCCESS.getPrefix() +
                    "§7- §aBecame: §f" + getPlayerName() + "§a on §f" + format.format(new Date(getChangeTime()));
        }

    }

}