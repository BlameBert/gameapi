package com.berttowne.gameapi.util;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.enchantments.EnchantmentWrapper;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Field;

public class GlowUtil extends EnchantmentWrapper {

    private static Enchantment glow;

    public GlowUtil(int id) {
        super(id);
    }

    public boolean canEnchantItem(ItemStack item) {
        return true;
    }

    public boolean conflictsWith(Enchantment other) {
        return false;
    }

    public EnchantmentTarget getItemTarget() {
        return null;
    }

    public int getMaxLevel() {
        return 10;
    }

    public int getStartLevel() {
        return 1;
    }

    public String getName() {
        return "Glow";
    }

    public static Enchantment getGlow() {
        if (glow != null) {
            return glow;
        }

        try {
            Field f = Enchantment.class.getDeclaredField("acceptingNew");
            f.setAccessible(true);
            f.set(null, true);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        glow = new GlowUtil(255);

        try {
            Enchantment.registerEnchantment(glow);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return glow;
    }

    public static void addGlow(ItemStack item) {
        Enchantment glow = getGlow();

        item.addUnsafeEnchantment(glow, 1);
    }

}