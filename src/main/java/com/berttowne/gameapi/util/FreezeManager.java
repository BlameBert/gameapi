package com.berttowne.gameapi.util;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerToggleSprintEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class FreezeManager implements Listener {

    private static List<UUID> players = new ArrayList<UUID>();
    private Plugin plugin;

    public FreezeManager(Plugin plugin) {
        this.plugin = plugin;
        this.plugin.getServer().getPluginManager().registerEvents(this, this.plugin);
    }

    /**
     * Freezes a player.
     */
    public void freezePlayer(Player p) {
        if (isFrozen(p)) return;
        p.setWalkSpeed(0F);
        p.setFallDistance(0F);
        p.teleport(this.getGround(p));
        p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, -127));
        players.add(p.getUniqueId());
    }

    /**
     * Unfreezes a player.
     */
    public void unfreezePlayer(Player p) {
        if (!isFrozen(p)) return;
        p.removePotionEffect(PotionEffectType.JUMP);
        p.setWalkSpeed(0.2F); // normal speed
        players.remove(p.getUniqueId());
    }

    private Location getGround(Player p) {
        Block relative = p.getLocation().getBlock().getRelative(BlockFace.DOWN);
        for (int i = 0; i < 256; i++) {
            if (relative.getType().isOccluding()) {
                Location b = relative.getRelative(BlockFace.UP).getLocation();
                return new Location(b.getWorld(), b.getX(), b.getY(), b.getZ(), p.getEyeLocation().getYaw(), p.getEyeLocation().getPitch()).add(0.5, 0, 0.5);
            } else {
                relative = relative.getRelative(BlockFace.DOWN);
            }
        }
        return p.getLocation();
    }

    /**
     * Check if a player is frozen.
     */
    public boolean isFrozen(Player p) {
        return players.contains(p.getUniqueId());
    }

    @EventHandler
    public void onSprint(PlayerToggleSprintEvent event) {
        final Player p = event.getPlayer();
        if (isFrozen(p)) {
            new BukkitRunnable() {
                @Override
                public void run() {
                    if (p.isSprinting()) {
                        p.teleport(p);
                    } else {
                        this.cancel();
                    }
                }
            }.runTaskTimer(this.plugin, 0L, 1L);
        }
    }

}