package com.berttowne.gameapi.util;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class BorderUtil {

    private static Method handle, sendPacket;
    private static Method center, distance, time, movement;
    private static Field player_connection;
    private static Constructor<?> constructor, border_constructor;
    private static Object constant;

    static {
        try {
            handle = getClass("org.bukkit.craftbukkit", "entity.CraftPlayer").getMethod("getHandle");
            player_connection = getClass("net.minecraft.server", "EntityPlayer").getField("playerConnection");
            for (Method m : getClass("net.minecraft.server", "PlayerConnection").getMethods()) {
                if (m.getName().equals("sendPacket")) {
                    sendPacket = m;
                    break;
                }
            }
            Class<?> enumclass;
            try {
                enumclass = getClass("net.minecraft.server", "EnumWorldBorderAction");
            } catch (ClassNotFoundException x) {
                enumclass = getClass("net.minecraft.server", "PacketPlayOutWorldBorder$EnumWorldBorderAction");
            }
            constructor = getClass("net.minecraft.server", "PacketPlayOutWorldBorder").getConstructor(getClass("net.minecraft.server", "WorldBorder"), enumclass);
            border_constructor = getClass("net.minecraft.server", "WorldBorder").getConstructor();

            Method[] methods = getClass("net.minecraft.server", "WorldBorder").getMethods();

            String setCenter = "setCenter";
            String setWarningDistance = "setWarningDistance";
            String setWarningTime = "setWarningTime";
            String transitionSizeBetween = "transitionSizeBetween";

            if (!inClass(methods, setCenter))
                setCenter = "c";
            if (!inClass(methods, setWarningDistance))
                setWarningDistance = "c";
            if (!inClass(methods, setWarningTime))
                setWarningTime = "b";
            if (!inClass(methods, transitionSizeBetween))
                transitionSizeBetween = "a";

            center = getClass("net.minecraft.server", "WorldBorder").getMethod(setCenter, double.class, double.class);
            distance = getClass("net.minecraft.server", "WorldBorder").getMethod(setWarningDistance, int.class);
            time = getClass("net.minecraft.server", "WorldBorder").getMethod(setWarningTime, int.class);
            movement = getClass("net.minecraft.server", "WorldBorder").getMethod(transitionSizeBetween, double.class, double.class, long.class);

            for (Object o : enumclass.getEnumConstants()) {
                if (o.toString().equals("INITIALIZE")) {
                    constant = o;
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static boolean inClass(Method[] methods, String methodName) {
        for (Method m : methods)
            if (m.getName().equals(methodName))
                return true;
        return false;
    }

    private static Class<?> getClass(String prefix, String name) throws Exception {
        return Class.forName((prefix + ".") + Bukkit.getServer().getClass().getPackage().getName().substring(Bukkit.getServer().getClass().getPackage().getName().lastIndexOf(".") + 1) + "." + name);
    }

    public static void sendRedZone(Player p, int percentage) {
        percentage = Math.round(percentage / 2);
        setBorder(p, percentage);
    }

    public static void fadeBorder(Player p, int percentage, long time) {
        int dist = -10000 * percentage + 1300000;
        sendWorldBorderPacket(p, 0, 200000D, (double) dist, (long) 1000 * time + 4000); //Add 4000 to make sure the "security" zone does not count in the fade time
    }

    public static void removeBorder(Player p) {
        sendWorldBorderPacket(p, 0, 200000D, 200000D, 0);
    }

    public static void setBorder(Player p, int percentage) {
        int dist = -10000 * percentage + 1300000;
        sendWorldBorderPacket(p, dist, 200000D, 200000D, 0);
    }

    public static void sendWorldBorderPacket(Player p, int dist, double oldradius, double newradius, long delay, Location location) {
        try {
            Object worldborder = border_constructor.newInstance();
            center.invoke(worldborder, location.getX(), location.getZ());
            distance.invoke(worldborder, dist);
            time.invoke(worldborder, 15);
            movement.invoke(worldborder, oldradius, newradius, delay);

            Object packet = constructor.newInstance(worldborder, constant);
            sendPacket.invoke(player_connection.get(handle.invoke(p)), packet);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void sendWorldBorderPacket(Player p, int dist, double oldradius, double newradius, long delay) {
        try {
            Object worldborder = border_constructor.newInstance();
            center.invoke(worldborder, p.getLocation().getX(), p.getLocation().getZ());
            distance.invoke(worldborder, dist);
            time.invoke(worldborder, 15);
            movement.invoke(worldborder, oldradius, newradius, delay);

            Object packet = constructor.newInstance(worldborder, constant);
            sendPacket.invoke(player_connection.get(handle.invoke(p)), packet);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}