package com.berttowne.gameapi.player;

import org.bukkit.ChatColor;

public enum Rank {

    GUEST("Guest", "§7", ChatColor.GRAY, 0),
    VIP("VIP", "§e§lVIP §e", ChatColor.WHITE, 1),
    MODERATOR("Mod", "§6§lMOD §6", ChatColor.WHITE, 2),
    ADMINISTRATOR("Admin", "§c§lADMIN §c", ChatColor.WHITE, 3),
    OWNER("Owner", "§4§lOWNER §4", ChatColor.GOLD, 4);

    private String name;
    private String prefix;
    private ChatColor chatColor;
    private int id;

    Rank(String name, String prefix, ChatColor chatColor, int id) {
        this.name = name;
        this.prefix = prefix;
        this.chatColor = chatColor;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getPrefix() {
        return prefix;
    }

    public ChatColor getChatColor() {
        return chatColor;
    }

    public int getId() {
        return id;
    }

    public static Rank getById(int id) {
        for (Rank rank : Rank.values()) {
            if (rank.id == id) {
                return rank;
            }
        }
        return null;
    }

    public static String printAllRanks() {
        String s = "";
        for (Rank r : values()) {
            s +=  r.getPrefix() + "§7, ";
        }
        return s;
    }

}