package com.berttowne.gameapi.player;

import com.berttowne.gameapi.Core;
import com.berttowne.gameapi.npc.PlayerNPC;
import com.berttowne.gameapi.util.MsgUtil;
import com.berttowne.gameapi.util.fetchers.UUIDFetcher;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.*;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class PlayerManager {

    private List<GamePlayer> onlinePlayers;

    public PlayerManager() {
        this.onlinePlayers = new ArrayList<>();
    }

    public List<GamePlayer> getOnlinePlayers() {
        return onlinePlayers;
    }

    public GamePlayer getPlayer(String name) {
        if (name.equalsIgnoreCase(" ") || name.equalsIgnoreCase("")) {
            return null;
        }

        for (PlayerNPC playerNPC : PlayerNPC.getRegisteredNPCs()) {
            if (((Entity) playerNPC.getEntity()).getName().equalsIgnoreCase(name)) {
                return null;
            }
        }

        FutureTask<GamePlayer> task = new FutureTask<>(() -> {
            for (GamePlayer p : onlinePlayers) {
                Player player = Core.getPlugin().getServer().getPlayer(name);
                if (player != null && p.getName().equals(player.getName())) {
                    return p;
                }
            }

            String uuid;
            if (Bukkit.getOfflinePlayer(name) != null) {
                uuid = Bukkit.getOfflinePlayer(name).getUniqueId().toString();
            } else {
                uuid = UUIDFetcher.getUUIDOf(name);
            }

            if (uuid != null && !uuid.equals("")) {
                Connection con = Core.getConnection();
                String sql = "SELECT * FROM players WHERE name = ?";

                try {
                    PreparedStatement stmt = con.prepareStatement(sql);
                    stmt.setString(1, name);
                    ResultSet set = stmt.executeQuery();

                    if (set.next()) {

                        int rank = set.getInt("rank");
                        int souls = set.getInt("souls");
                        int shards = set.getInt("shards");

                        GamePlayer p = new GamePlayer();

                        p.setName(name);
                        p.setUUID(uuid);

                        Rank r = Rank.getById(rank);

                        if (r != null) {
                            p.setRank(r);
                        } else {
                            p.setRank(Rank.GUEST);
                        }

                        p.setSouls(souls);
                        p.setShards(shards);

                        add(p);
                        return p;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

            return null;
        });

        new Thread(task).start();

        try {
            return task.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return null;
    }

    public GamePlayer createPlayer(String name) {
        FutureTask<GamePlayer> task = new FutureTask<>(() -> {
            Connection con = Core.getConnection();

            String uuid;
            if (Bukkit.getOfflinePlayer(name) != null) {
                uuid = Bukkit.getOfflinePlayer(name).getUniqueId().toString();
            } else {
                uuid = UUIDFetcher.getUUIDOf(name);
            }

            if (uuid != null && !uuid.equals("")) {
                if (getPlayer(name) == null) {
                    String sql = "INSERT INTO `players`(name, uuid, souls, shards, rank, banned, muted) VALUES (?, ?, 0, 0, 0, 0, 0)";

                    try {
                        PreparedStatement stmt = con.prepareStatement(sql);
                        stmt.setString(1, name);
                        stmt.setString(2, uuid);
                        stmt.executeUpdate();
                        stmt.close();

                        return getPlayer(name);
                    } catch (Exception e) {
                        e.printStackTrace();
                        return null;
                    }
                }

                try {
                    PreparedStatement stmt = con.prepareStatement("SELECT * FROM players WHERE uuid = ?");
                    stmt.setString(1, uuid);
                    ResultSet set = stmt.executeQuery();

                    if (set.next()) {
                        int rank = set.getInt("rank");
                        int souls = set.getInt("souls");
                        int shards = set.getInt("shards");

                        GamePlayer p = new GamePlayer();

                        p.setName(name);
                        p.setUUID(uuid);

                        Rank r = Rank.getById(rank);

                        if (r != null) {
                            p.setRank(r);
                        } else {
                            p.setRank(Rank.GUEST);
                        }

                        p.setSouls(souls);
                        p.setShards(shards);

                        if (!onlinePlayers.contains(p)) {
                            add(p);
                        }
                        return p;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

            return null;
        });

        new Thread(task).start();

        try {
            return task.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Rank getRank(String player) {
        FutureTask<Rank> task = new FutureTask<>(() -> {
            String uuid;
            if (Bukkit.getOfflinePlayer(player) != null) {
                uuid = Bukkit.getOfflinePlayer(player).getUniqueId().toString();
            } else {
                uuid = UUIDFetcher.getUUIDOf(player);
            }

            if (uuid != null && !uuid.equals("")) {
                Connection con = Core.getConnection();
                String sql = "SELECT rank FROM players WHERE uuid = ?;";

                try {
                    PreparedStatement stmt = con.prepareStatement(sql);
                    stmt.setString(1, uuid);

                    ResultSet set = stmt.executeQuery();
                    if (set.next()) {
                        return Rank.getById(set.getInt(1));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return null;
        });

        new Thread(task).start();

        try {
            return task.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void setRank(String player, Rank rank) {
        this.setInteger("players", "rank", player, rank.getId());
    }

    public int getSouls(String player) {
        FutureTask<Integer> task = new FutureTask<>(() -> {
            String uuid;
            if (Bukkit.getOfflinePlayer(player) != null) {
                uuid = Bukkit.getOfflinePlayer(player).getUniqueId().toString();
            } else {
                uuid = UUIDFetcher.getUUIDOf(player);
            }

            if (uuid != null && !uuid.equals("")) {
                Connection con = Core.getConnection();
                String sql = "SELECT souls FROM players WHERE uuid = ?";

                try {
                    PreparedStatement stmt = con.prepareStatement(sql);
                    stmt.setString(1, uuid);
                    ResultSet set = stmt.executeQuery();

                    if (set.next()) {
                        return set.getInt(1);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return 0;
        });

        new Thread(task).start();

        try {
            return task.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return 0;
    }

    public void setSouls(String player, int souls) {
        this.setInteger("players", "souls", player, souls);
    }

    public int getShards(String player) {
        FutureTask<Integer> task = new FutureTask<>(() -> {
            String uuid;
            if (Bukkit.getOfflinePlayer(player) != null) {
                uuid = Bukkit.getOfflinePlayer(player).getUniqueId().toString();
            } else {
                uuid = UUIDFetcher.getUUIDOf(player);
            }

            if (uuid != null && !uuid.equals("")) {
                Connection con = Core.getConnection();
                String sql = "SELECT shards FROM players WHERE uuid = ?";

                try {
                    PreparedStatement stmt = con.prepareStatement(sql);
                    stmt.setString(1, uuid);
                    ResultSet set = stmt.executeQuery();

                    if (set.next()) {
                        return set.getInt(1);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return 0;
        });

        new Thread(task).start();

        try {
            return task.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return 0;
    }

    public void setShards(String player, int shards) {
        this.setInteger("players", "shards", player, shards);
    }

    public boolean isBanned(String player) {
        FutureTask<Boolean> task = new FutureTask<>(() -> {
            String uuid;
            if (Bukkit.getOfflinePlayer(player) != null) {
                uuid = Bukkit.getOfflinePlayer(player).getUniqueId().toString();
            } else {
                uuid = UUIDFetcher.getUUIDOf(player);
            }

            if (uuid != null && !uuid.equals("")) {
                Connection con = Core.getConnection();

                boolean l1 = false;
                boolean l2 = false;

                boolean banned = false;

                try {
                    PreparedStatement stmt = con.prepareStatement("SELECT banned FROM players WHERE uuid = ?");
                    stmt.setString(1, uuid);
                    ResultSet set = stmt.executeQuery();

                    if (set.next()) {
                        l1 = set.getInt(1) == 1;
                    }
                    stmt.executeQuery();
                } catch (Exception e) {
                    e.printStackTrace();
                    l1 = false;
                }

                try {
                    PreparedStatement stmt = con.prepareStatement("SELECT active FROM bans WHERE uuid = ?");
                    stmt.setString(1, uuid);
                    ResultSet set = stmt.executeQuery();

                    if (set.next()) {
                        l2 = set.getInt(1) == 1;
                    }
                    stmt.executeQuery();
                } catch (Exception e) {
                    e.printStackTrace();
                    l2 = false;
                }

                banned = l1 && l2 || l1 && !l2;

                if (banned) {
                    Duration duration = null;

                    LocalDateTime today = LocalDateTime.now();
                    LocalDateTime issued = null;
                    LocalDateTime ended = null;

                    try {
                        PreparedStatement stmt = con.prepareStatement("SELECT date_issued FROM bans WHERE uuid = ?");
                        stmt.setString(1, uuid);
                        ResultSet set = stmt.executeQuery();

                        if (set.next()) {
                            issued = set.getTimestamp(1).toLocalDateTime();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {
                        PreparedStatement stmt = con.prepareStatement("SELECT date_ended FROM bans WHERE uuid = ?");
                        stmt.setString(1, uuid);
                        ResultSet set = stmt.executeQuery();

                        if (set.next()) {
                            ended = set.getTimestamp(1).toLocalDateTime();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (issued != null && ended != null) {
                        if (Duration.between(issued, ended).isZero()) { // Banned forever
                            return true;
                        }

                        if (Duration.between(today, ended).isZero() || Duration.between(today, ended).isNegative()) { // Ban expired
                            unban(player, "Console");
                            return false;
                        } else {
                            return true;
                        }
                    } else {
                        return false;
                    }
                }
            }

            return false;
        });

        new Thread(task).start();

        try {
            return task.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return false;
    }

    public String getBanString(String player) {
        if (isBanned(player)) {
            FutureTask<String> task = new FutureTask<>(() -> {
                String uuid;
                if (Bukkit.getOfflinePlayer(player) != null) {
                    uuid = Bukkit.getOfflinePlayer(player).getUniqueId().toString();
                } else {
                    uuid = UUIDFetcher.getUUIDOf(player);
                }

                if (uuid != null && !uuid.equals("")) {
                    Connection con = Core.getConnection();

                    Duration duration;
                    String banner = "";
                    String reason = "";
                    String formattedTime = "";

                    try {
                        PreparedStatement stmt = con.prepareStatement("SELECT date_ended FROM bans WHERE uuid = ?");
                        stmt.setString(1, uuid);
                        ResultSet set = stmt.executeQuery();

                        if (set.next()) {
                            LocalDateTime today = LocalDateTime.now();
                            LocalDateTime ending = set.getTimestamp(1).toLocalDateTime();

                            duration = Duration.between(today, ending);
                            formattedTime = DurationFormatUtils.formatDurationWords(duration.toMillis(), true, false);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {
                        PreparedStatement stmt = con.prepareStatement("SELECT banner FROM bans WHERE uuid = ?");
                        stmt.setString(1, uuid);
                        ResultSet set = stmt.executeQuery();

                        if (set.next()) {
                            banner = set.getString(1);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {
                        PreparedStatement stmt = con.prepareStatement("SELECT reason FROM bans WHERE uuid = ?");
                        stmt.setString(1, uuid);
                        ResultSet set = stmt.executeQuery();

                        if (set.next()) {
                            reason = set.getString(1);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    return "§4§lYOU HAVE BEEN BANNED!\n§cReason: §c§o" + reason + "\n§cBy: §c§o" + banner + "\n§cDuration: §c§o" + formattedTime;
                }

                return null;
            });

            new Thread(task).start();

            try {
                return task.get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }

            return null;
        }
        return null;
    }

    public void ban(String player, String reason, String banner, Duration duration) {
        new BukkitRunnable() {
            @Override
            public void run() {
                String uuid;
                if (Bukkit.getOfflinePlayer(player) != null) {
                    uuid = Bukkit.getOfflinePlayer(player).getUniqueId().toString();
                } else {
                    uuid = UUIDFetcher.getUUIDOf(player);
                }

                if (uuid != null && !uuid.equals("")) {
                    Connection con = Core.getConnection();

                    String formattedTime = "";

                    if (!duration.isZero()) {
                        formattedTime = DurationFormatUtils.formatDurationWords(duration.toMillis(), true, false);
                    } else {
                        formattedTime = "Forever";
                    }

                    try {
                        PreparedStatement stmt = con.prepareStatement("UPDATE players SET banned = ? WHERE uuid = ?;");
                        stmt.setInt(1, 1);
                        stmt.setString(2, uuid);
                        stmt.executeUpdate();
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }

                    try {
                        PreparedStatement stmt = con.prepareStatement(
                                "INSERT INTO `bans`(uuid, date_ended, active, reason, banner) VALUES (?, ?, ?, ?, ?)");

                        LocalDateTime today = LocalDateTime.now();
                        LocalDateTime ending = today.plus(duration);
                        Timestamp timestamp = Timestamp.valueOf(ending);

                        stmt.setString(1, uuid);
                        stmt.setTimestamp(2, timestamp);
                        stmt.setInt(3, 1);
                        stmt.setString(4, reason);
                        stmt.setString(5, banner);
                        stmt.executeUpdate();
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }

                    Core.getPlugin().getServer().broadcastMessage(MsgUtil.MsgType.DANGER.getPrefix() + player + " has been banned by " + banner +
                            " for " + reason + " " + "(" + formattedTime + ")");
                }
            }
        }.runTaskAsynchronously(Core.getPlugin());

        if (Core.getPlugin().getServer().getPlayer(player).isOnline()) {
            Core.getPlugin().getServer().getPlayer(player).kickPlayer("§4§lYOU HAVE BEEN BANNED!\n§cReason: §c§o" + reason + "\n§cBy: §c§o" + banner +
                    "\n§cDuration: §c§o" + getBanString(player));
        }
    }

    public void unban(String player, String unbanner) {
        new BukkitRunnable() {
            @Override
            public void run() {
                Connection con = Core.getConnection();

                if (isBanned(player)) {
                    String uuid;
                    if (Bukkit.getOfflinePlayer(player) != null) {
                        uuid = Bukkit.getOfflinePlayer(player).getUniqueId().toString();
                    } else {
                        uuid = UUIDFetcher.getUUIDOf(player);
                    }

                    if (uuid != null && !uuid.equals("")) {
                        try {
                            PreparedStatement stmt = con.prepareStatement("UPDATE players SET banned = ? WHERE uuid = ?;");
                            stmt.setInt(1, 0);
                            stmt.setString(2, uuid);
                            stmt.executeUpdate();
                        } catch (Exception e) {
                            e.printStackTrace();
                            return;
                        }

                        try {
                            PreparedStatement stmt = con.prepareStatement("UPDATE bans SET active = ? WHERE uuid = ?;");
                            stmt.setInt(1, 0);
                            stmt.setString(2, uuid);
                            stmt.executeUpdate();
                        } catch (Exception e) {
                            e.printStackTrace();
                            return;
                        }

                        Core.getPlugin().getServer().broadcastMessage(MsgUtil.MsgType.WARNING.getPrefix() + player + " has been unbanned by " + unbanner);
                    }
                }
            }
        }.runTaskAsynchronously(Core.getPlugin());
    }

    public void kick(String player, String reason, String kicker) {
        String uuid;
        if (Bukkit.getOfflinePlayer(player) != null) {
            uuid = Bukkit.getOfflinePlayer(player).getUniqueId().toString();
        } else {
            uuid = UUIDFetcher.getUUIDOf(player);
        }

        if (uuid != null && !uuid.equals("")) {
            Core.getPlugin().getServer().getPlayer(player).kickPlayer("§4§lYOU HAVE BEEN KICKED!\n§cReason: §c§o" + reason + "\n§cBy: §c§o" + kicker);

            new BukkitRunnable() {
                @Override
                public void run() {
                    Connection con = Core.getConnection();
                    try {
                        PreparedStatement stmt = con.prepareStatement(
                                "INSERT INTO `kicks`(uuid, reason, kicker) VALUES (?, ?, ?)");

                        stmt.setString(1, uuid);
                        stmt.setString(2, reason);
                        stmt.setString(3, kicker);
                        stmt.executeUpdate();
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }

                    Core.getPlugin().getServer().broadcastMessage(MsgUtil.MsgType.WARNING.getPrefix() + player + " has been kicked by " + kicker + " for " +
                            reason);
                }
            }.runTaskAsynchronously(Core.getPlugin());
        }
    }

    public boolean isMuted(String player) {
        FutureTask<Boolean> task = new FutureTask<>(() -> {
            String uuid;
            if (Bukkit.getOfflinePlayer(player) != null) {
                uuid = Bukkit.getOfflinePlayer(player).getUniqueId().toString();
            } else {
                uuid = UUIDFetcher.getUUIDOf(player);
            }

            if (uuid != null && !uuid.equals("")) {
                Connection con = Core.getConnection();

                boolean l1 = false;
                boolean l2 = false;

                boolean muted;

                try {
                    PreparedStatement stmt = con.prepareStatement("SELECT muted FROM players WHERE uuid = ?");
                    stmt.setString(1, uuid);
                    ResultSet set = stmt.executeQuery();

                    if (set.next()) {
                        l1 = set.getInt(1) == 1;
                    }
                    stmt.executeQuery();
                } catch (Exception e) {
                    e.printStackTrace();
                    l1 = false;
                }

                try {
                    PreparedStatement stmt = con.prepareStatement("SELECT active FROM mutes WHERE uuid = ?");
                    stmt.setString(1, uuid);
                    ResultSet set = stmt.executeQuery();

                    if (set.next()) {
                        l2 = set.getInt(1) == 1;
                    }
                    stmt.executeQuery();
                } catch (Exception e) {
                    e.printStackTrace();
                    l2 = false;
                }

                muted = l1 && l2 || l1 && !l2;

                if (muted) {
                    Duration duration = null;

                    LocalDateTime today = LocalDateTime.now();
                    LocalDateTime issued = null;
                    LocalDateTime ended = null;

                    try {
                        PreparedStatement stmt = con.prepareStatement("SELECT date_issued FROM mutes WHERE uuid = ?");
                        stmt.setString(1, uuid);
                        ResultSet set = stmt.executeQuery();

                        if (set.next()) {
                            issued = set.getTimestamp(1).toLocalDateTime();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {
                        PreparedStatement stmt = con.prepareStatement("SELECT date_ended FROM mutes WHERE uuid = ?");
                        stmt.setString(1, uuid);
                        ResultSet set = stmt.executeQuery();

                        if (set.next()) {
                            ended = set.getTimestamp(1).toLocalDateTime();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (issued != null && ended != null) {
                        if (Duration.between(issued, ended).isZero()) { // Muted forever
                            return true;
                        }

                        if (Duration.between(today, ended).isZero() || Duration.between(today, ended).isNegative()) { // Mute expired
                            unmute(player, "Console");
                            return false;
                        } else {
                            return true;
                        }
                    } else {
                        return false;
                    }
                }
            }

            return false;
        });

        new Thread(task).start();

        try {
            return task.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return false;
    }

    public String getMuteString(String player) {
        FutureTask<String> task = new FutureTask<>(() -> {
            if (isMuted(player)) {
                String uuid;
                if (Bukkit.getOfflinePlayer(player) != null) {
                    uuid = Bukkit.getOfflinePlayer(player).getUniqueId().toString();
                } else {
                    uuid = UUIDFetcher.getUUIDOf(player);
                }

                if (uuid != null && !uuid.equals("")) {
                    Connection con = Core.getConnection();

                    Duration duration;
                    String muter = "";
                    String reason = "";
                    String formattedTime = "";

                    try {
                        PreparedStatement stmt = con.prepareStatement("SELECT date_ended FROM mutes WHERE uuid = ?");
                        stmt.setString(1, uuid);
                        ResultSet set = stmt.executeQuery();

                        if (set.next()) {
                            LocalDateTime today = LocalDateTime.now();
                            LocalDateTime ending = set.getTimestamp(1).toLocalDateTime();

                            duration = Duration.between(today.toLocalDate(), ending.toLocalDate());
                            formattedTime = DurationFormatUtils.formatDurationWords(duration.toMillis(), true, false);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {
                        PreparedStatement stmt = con.prepareStatement("SELECT muter FROM mutes WHERE uuid = ?");
                        stmt.setString(1, uuid);
                        ResultSet set = stmt.executeQuery();

                        if (set.next()) {
                            muter = set.getString(1);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {
                        PreparedStatement stmt = con.prepareStatement("SELECT reason FROM mutes WHERE uuid = ?");
                        stmt.setString(1, uuid);
                        ResultSet set = stmt.executeQuery();

                        if (set.next()) {
                            reason = set.getString(1);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    return MsgUtil.MsgType.DANGER.getPrefix() + "You have been muted!\n"
                            + MsgUtil.MsgType.DANGER.getPrefix() + "By: §c§o" + muter + "\n"
                            + MsgUtil.MsgType.DANGER.getPrefix() + "Reason: §c§o" + reason + "\n"
                            + MsgUtil.MsgType.DANGER.getPrefix() + "Duration: §c§o" + formattedTime;
                }
            }

            return null;
        });

        new Thread(task).start();

        try {
            return task.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void mute(String player, String reason, String muter, Duration duration) {
        new BukkitRunnable() {
            @Override
            public void run() {
                String uuid;
                if (Bukkit.getOfflinePlayer(player) != null) {
                    uuid = Bukkit.getOfflinePlayer(player).getUniqueId().toString();
                } else {
                    uuid = UUIDFetcher.getUUIDOf(player);
                }

                Connection con = Core.getConnection();

                try {
                    PreparedStatement stmt = con.prepareStatement("UPDATE players SET muted = ? WHERE uuid = ?;");
                    stmt.setInt(1, 1);
                    stmt.setString(2, uuid);
                    stmt.executeUpdate();
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }

                try {
                    PreparedStatement stmt = con.prepareStatement(
                            "INSERT INTO `mutes`(uuid, date_ended, active, reason, muter) VALUES (?, ?, ?, ?, ?)");

                    LocalDateTime today = LocalDateTime.now();
                    LocalDateTime ending = today.plus(duration);
                    Timestamp timestamp = Timestamp.valueOf(ending);

                    stmt.setString(1, uuid);
                    stmt.setTimestamp(2, timestamp);
                    stmt.setInt(3, 1);
                    stmt.setString(4, reason);
                    stmt.setString(5, muter);
                    stmt.executeUpdate();
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }

                if (Core.getPlugin().getServer().getPlayer(player).isOnline()) {
                    Player p = Core.getPlugin().getServer().getPlayer(player);

                    p.sendMessage(MsgUtil.MsgType.DANGER.getPrefix() + "You have been muted!");
                    p.sendMessage(MsgUtil.MsgType.DANGER.getPrefix() + "By: §c§o" + muter);
                    p.sendMessage(MsgUtil.MsgType.DANGER.getPrefix() + "Reason: §c§o" + reason);
                    p.sendMessage(MsgUtil.MsgType.DANGER.getPrefix() +
                            "Duration: §c§o" + DurationFormatUtils.formatDurationWords(duration.toMillis(), true, false));
                }

                Core.getPlugin().getServer().broadcastMessage(MsgUtil.MsgType.DANGER.getPrefix() + player + " has been muted by " + muter +
                        " for " + reason + " " + "(" + DurationFormatUtils.formatDurationWords(duration.toMillis(), true, false) + ")");
            }
        }.runTaskAsynchronously(Core.getPlugin());
    }

    public void unmute(String player, String unmuter) {
        new BukkitRunnable() {
            @Override
            public void run() {
                String uuid;
                if (Bukkit.getOfflinePlayer(player) != null) {
                    uuid = Bukkit.getOfflinePlayer(player).getUniqueId().toString();
                } else {
                    uuid = UUIDFetcher.getUUIDOf(player);
                }

                Connection con = Core.getConnection();

                if (isMuted(player)) {
                    try {
                        PreparedStatement stmt = con.prepareStatement("UPDATE players SET muted = ? WHERE uuid = ?;");
                        stmt.setInt(1, 0);
                        stmt.setString(2, uuid);
                        stmt.executeUpdate();
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }

                    try {
                        PreparedStatement stmt = con.prepareStatement("UPDATE mutes SET active = ? WHERE uuid = ?;");
                        stmt.setInt(1, 0);
                        stmt.setString(2, uuid);
                        stmt.executeUpdate();
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }

                    if (Core.getPlugin().getServer().getPlayer(player).isOnline()) {
                        Core.getPlugin().getServer().getPlayer(player)
                                .sendMessage(MsgUtil.MsgType.INFO.getPrefix() + "You have been unmuted by §b§o" + unmuter + "§b!");
                    }

                    Core.getPlugin().getServer().broadcastMessage(MsgUtil.MsgType.WARNING.getPrefix() + player + " has been unmuted by " + unmuter);
                }
            }
        }.runTaskAsynchronously(Core.getPlugin());
    }

    private void setInteger(String table, String column, String player, int amount) {
        new BukkitRunnable() {
            @Override
            public void run() {
                String uuid;
                if (Bukkit.getOfflinePlayer(player) != null) {
                    uuid = Bukkit.getOfflinePlayer(player).getUniqueId().toString();
                } else {
                    uuid = UUIDFetcher.getUUIDOf(player);
                }

                if (uuid != null && !uuid.equals("")) {
                    Connection con = Core.getConnection();
                    String sql = "UPDATE `" + table + "` set " + column + " = ? WHERE uuid = ?";

                    try {
                        PreparedStatement stmt = con.prepareStatement(sql);
                        stmt.setInt(1, amount);
                        stmt.setString(2, uuid);
                        stmt.executeUpdate();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }.runTaskAsynchronously(Core.getPlugin());
    }

    private void add(GamePlayer player) {
        if (!contains(player)) {
            onlinePlayers.add(player);
        }
    }

    private boolean contains(GamePlayer player) {
        for (GamePlayer p : onlinePlayers) {
            if (p.getName().equals(player.getName())) {
                return true;
            }
        }
        return false;
    }

}