package com.berttowne.gameapi.player;

import com.berttowne.gameapi.Core;
import com.berttowne.gameapi.util.MsgUtil;
import org.bukkit.Bukkit;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.net.InetSocketAddress;

public class GamePlayer {

    private String name;
    private String uuid;

    private Rank rank;

    private int shards;
    private int souls;

    private boolean adminMode = false;

    public boolean isAdminMode() {
        return adminMode;
    }

    public void setAdminMode(boolean adminMode) {
        this.adminMode = adminMode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUUID() {
        return uuid;
    }

    public void setUUID(String uuid) {
        this.uuid = uuid;
    }

    public Rank getRank() {
        return Core.getPlayerManager().getRank(name);
    }

    public void setRank(Rank rank) {
        Bukkit.getScheduler().runTaskAsynchronously(Core.getPlugin(), () ->  {
            Core.getPlayerManager().setRank(name, rank);
            this.rank = rank;
        });
    }

    public int getShards() {
        return Core.getPlayerManager().getShards(name);
    }

    public void setShards(int shards) {
        Bukkit.getScheduler().runTaskAsynchronously(Core.getPlugin(), () -> {
            Core.getPlayerManager().setShards(name, shards);
            this.shards = shards;
        });
    }

    public int getSouls() {
        return Core.getPlayerManager().getSouls(name);
    }

    public void setSouls(int souls) {
        Bukkit.getScheduler().runTaskAsynchronously(Core.getPlugin(), () -> {
            Core.getPlayerManager().setSouls(name, souls);
            this.souls = souls;
        });
    }

    public void message(String message) {
        if (Core.getPlugin().getServer().getPlayer(name) != null) {
            Core.getPlugin().getServer().getPlayer(name).sendMessage(message);
        }
    }

    public void message(MsgUtil.MsgType type, String message) {
        if (Core.getPlugin().getServer().getPlayer(name) != null) {
            Core.getPlugin().getServer().getPlayer(name).sendMessage(type.getPrefix() + message);
        }
    }

    public InetSocketAddress getAddress() {
        if (Core.getPlugin().getServer().getPlayer(uuid).isOnline()) {
            return Core.getPlugin().getServer().getPlayer(uuid).getAddress();
        }
        return null;
    }

    public void sendToServer(String targetServer) {
        if (Core.getPlugin().getServer().getPlayer(uuid).isOnline()) {
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            DataOutputStream out = new DataOutputStream(b);

            try {
                out.writeUTF("Connect");
                out.writeUTF(targetServer);
            } catch (Exception e) {
                e.printStackTrace();
            }

            Core.getPlugin().getServer().getPlayer(uuid)
                    .sendPluginMessage(Core.getPlugin(), "BungeeCord", b.toByteArray());
        }
    }

    public boolean isMuted() {
        return Core.getPlayerManager().isMuted(name);
    }

    public boolean isBanned() {
        return Core.getPlayerManager().isBanned(name);
    }

    public boolean matches(GamePlayer player) {
        return Core.getPlugin().getServer().getPlayer(uuid).isOnline() && this.name.equals(player.getName());
    }

}