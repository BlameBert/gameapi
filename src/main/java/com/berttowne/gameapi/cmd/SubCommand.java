package com.berttowne.gameapi.cmd;

import com.berttowne.gameapi.util.MsgUtil;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public abstract class SubCommand {

    private Command parentCommand;

    private final String childExecutor;
    private final List<String> aliases;

    private final String permissionNode;
    private boolean inheritPermissions;
    private String permissionMessage;

    public SubCommand(Command parent, String executor) {
        this(parent, null, new String[] { executor });
    }

    public SubCommand(Command parent, String... executors) {
        this(parent, null, executors);
    }

    public SubCommand(Command parent, String permissionNode, String... executors) {
        parentCommand = parent;
        this.permissionNode = permissionNode;
        childExecutor = executors[0];
        aliases = Arrays.asList(executors);

        permissionMessage = MsgUtil.MsgType.DANGER.getPrefix() + "You do not have permission to use that sub-command!";
    }

    public final Command parent() {
        return parentCommand;
    }

    public String executor() {
        return childExecutor;
    }

    public final List<String> aliases() {
        return Collections.unmodifiableList(aliases);
    }

    public final String permissionNode() {
        return permissionNode;
    }

    public SubCommand inheritPermissions(boolean val) {
        inheritPermissions = val;
        return this;
    }

    public boolean inheritPermissions() {
        return inheritPermissions;
    }

    public String message() {
        return permissionMessage;
    }

    public SubCommand message(String val) {
        permissionMessage = ChatColor.translateAlternateColorCodes('&', val);
        return this;
    }

    public boolean hasAlias(String attempt) {
        return aliases.stream().anyMatch(alias -> alias.equalsIgnoreCase(attempt));
    }

    public abstract void execute(Player player, String... args);

}