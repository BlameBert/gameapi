package com.berttowne.gameapi.cmd;

import com.google.common.base.Objects;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.Validate;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public abstract class Command {

    private final String mainExecutor;

    private final List<String> aliases;

    private final String permissionNode;

    private HashMap<String, SubCommand> childCommandRelation;

    public Command(CommandHandler handler, String executor) {
        this(handler, null, executor);
    }

    public Command(CommandHandler handler, String permission, String... executors) {
        Validate.isTrue(executors.length > 0, "You must provide at least one executor for this command to use!");

        mainExecutor = executors[0];
        aliases = Arrays.asList(executors);
        permissionNode = permission;
        childCommandRelation = new HashMap<>();

        handler.addCommand(this);
    }

    public final String executor() {
        return mainExecutor;
    }

    public final String permissionNode() {
        return permissionNode;
    }

    public final List<String> aliases() {
        return Collections.unmodifiableList(aliases);
    }

    public void addSubCommand(SubCommand command) {
        command.aliases().forEach(alias -> childCommandRelation.put(alias.toLowerCase(), command));
    }

    public abstract void execute(Player player, String... args);

    public void attemptSubCommand(final Player player, final String[] args) {
        if (args == null || args.length <= 0) return;

        String playerInput = args[0].toLowerCase();

        childCommandRelation.forEach((mainExecutor, command) -> {
            if (playerInput.equals(mainExecutor) || command.hasAlias(playerInput)) {
                if (command.permissionNode() != null || command.inheritPermissions()) {
                    if (!player.hasPermission(command.permissionNode())
                            || (command.inheritPermissions() && !player.hasPermission(permissionNode))) {
                        player.sendMessage(command.message());
                        return;
                    }
                }

                command.execute(player, (String[]) ArrayUtils.remove(args, 0));
            }
        });
    }

    public static boolean matches(String[] args, String... checks) {
        return matches(args[0], checks);
    }

    public static boolean matches(String arg, String... checks) {
        return Arrays.stream(checks).anyMatch(check -> check.equalsIgnoreCase(arg));
    }

    public static boolean matchesExact(String[] args, String... checks) {
        return matches(args[0], checks);
    }

    public static boolean matchesExact(String arg, String... checks) {
        return Arrays.stream(checks).anyMatch(check -> check.equals(arg));
    }

    @Override
    public String toString()
    {
        return Objects.toStringHelper(this)
                .add("executor", mainExecutor)
                .add("aliases", aliases)
                .add("permissionNode", permissionNode)
                .add("childCommands", Arrays.toString(childCommandRelation.keySet().toArray(new String[childCommandRelation.keySet().size()])))
                .toString();
    }

}