package com.berttowne.gameapi.cmd;

import com.berttowne.gameapi.util.MsgUtil;
import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Collection;
import java.util.HashMap;
import java.util.Optional;

public class CommandHandler implements Listener {

    private boolean hasStarted = false;

    private final JavaPlugin plugin;
    private final HashMap<String, Command> watchedCommands;

    private String missingPermissionMessage;

    public CommandHandler(JavaPlugin yourPlugin) {
        plugin = yourPlugin;
        watchedCommands = new HashMap<>();
        missingPermissionMessage = MsgUtil.MsgType.DANGER.getPrefix() + "You do not have permission to use that command!";

        plugin.getServer().getPluginManager().registerEvents(this, plugin);

        hasStarted = true;
    }

    public void end() {
        aliveCheck("end()");

        Bukkit.getLogger().info("Someone has disabled our command handler. May wanna check that out?");
        HandlerList.unregisterAll(this);
    }

    public JavaPlugin plugin() {
        aliveCheck("plugin()");

        return plugin;
    }

    public String message() {
        aliveCheck("message()");

        return missingPermissionMessage;
    }

    public CommandHandler message(String val) {
        aliveCheck("message(String)");

        missingPermissionMessage = ChatColor.translateAlternateColorCodes('&', val);
        return this;
    }

    public Command addCommand(Command command) {
        aliveCheck("addCommand(Command)");

        command.aliases().forEach(alias -> watchedCommands.put(alias.toLowerCase(), command));
        return command;
    }

    public CommandHandler addCommands(Command... commands) {
        aliveCheck("addCommands(Command...)");
        Validate.isTrue(commands.length >= 1, "You must provide at least one command! tsk tsk");

        for (Command command : commands)
            addCommand(command);

        return this;
    }

    public CommandHandler addCommands(Collection<Command> commands) {
        aliveCheck("addCommands(Collection<Command>)");

        commands.forEach(this::addCommand);
        return this;
    }

    public CommandHandler removeCommand(Command command) {
        aliveCheck("removeCommand(Command)");

        command.aliases().forEach(watchedCommands::remove);
        return this;
    }

    public Optional<Command> findCommand(String executor) {
        return Optional.ofNullable(watchedCommands.get(executor));
    }

    private void aliveCheck(String method) {
        if (!hasStarted)
            throw new UnsupportedOperationException("You must first create a 'CommandHandler' instance before adding cmds. " +
                    "\n Try adding `new CommandHandler(YourPlugin)` before any use of the method `" + method + "`.");
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.MONITOR)
    public void handleCommands(PlayerCommandPreprocessEvent event) {
        Player player = event.getPlayer();
        String commandRaw = event.getMessage().substring(1).toLowerCase();
        String[] args = null;

        if (commandRaw.contains(" ")) {
            commandRaw = commandRaw.split(" ")[0];
            args = event.getMessage().substring(event.getMessage().indexOf(' ') + 1).split(" ");
        }

        if (args == null) args = new String[0];

        if (watchedCommands.containsKey(commandRaw)) {
            event.setCancelled(true);

            Command command = watchedCommands.get(commandRaw);

            if (command.permissionNode() != null) {
                if (!player.hasPermission(command.permissionNode())) {
                    player.sendMessage(missingPermissionMessage);
                    return;
                }
            }

            command.execute(player, args);
        }
    }

}