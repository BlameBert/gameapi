package com.berttowne.gameapi;

import com.berttowne.gameapi.achievement.Achievement;
import com.berttowne.gameapi.achievement.AchievementHandler;
import com.berttowne.gameapi.cmd.CommandHandler;
import com.berttowne.gameapi.cmds.AdminModeCommand;
import com.berttowne.gameapi.cmds.DebugCommand;
import com.berttowne.gameapi.cmds.RankCommand;
import com.berttowne.gameapi.cmds.currency.ShardsCommand;
import com.berttowne.gameapi.cmds.currency.SoulsCommand;
import com.berttowne.gameapi.cmds.fetchers.NameCommand;
import com.berttowne.gameapi.cmds.fetchers.NamesCommand;
import com.berttowne.gameapi.cmds.fetchers.UUIDCommand;
import com.berttowne.gameapi.cmds.players.AchievementsCommand;
import com.berttowne.gameapi.cmds.punishments.*;
import com.berttowne.gameapi.damageutils.listeners.ConnectionListener;
import com.berttowne.gameapi.damageutils.listeners.DamageListener;
import com.berttowne.gameapi.damageutils.listeners.DeathListener;
import com.berttowne.gameapi.friends.FriendManager;
import com.berttowne.gameapi.listeners.AdminListener;
import com.berttowne.gameapi.listeners.ChatListener;
import com.berttowne.gameapi.listeners.JoinListener;
import com.berttowne.gameapi.mysql.MySQL;
import com.berttowne.gameapi.npc.CustomNPC;
import com.berttowne.gameapi.npc.PlayerNPC;
import com.berttowne.gameapi.player.PlayerManager;
import com.berttowne.gameapi.player.Rank;
import com.berttowne.gameapi.runnables.RankParticles;
import com.berttowne.gameapi.server.ServerData;
import com.berttowne.gameapi.server.ServerState;
import com.berttowne.gameapi.util.FreezeManager;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.bukkit.Material;
import org.bukkit.plugin.java.JavaPlugin;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPubSub;

import java.sql.Connection;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import java.util.stream.Stream;

public class Core extends JavaPlugin {

    private static Core plugin;

    private static FreezeManager freezeManager;
    private static PlayerManager playerManager;
    private static FriendManager friendManager;

    private static CommandHandler commandHandler;
    private static AchievementHandler achievementHandler;

    private static Connection connection;
    private static MySQL database;

    private static Connection acon;
    private static MySQL adb;

    private static final String REDIS_CHANNEL = "dynamic-server";

    private static JedisPool JEDIS_POOL;
    private static Gson GSON;

    private static ServerData serverData;

    private static Cache<String, ServerData> SERVER_CACHE = CacheBuilder.newBuilder()
            .expireAfterWrite(30, TimeUnit.SECONDS).build();

    @Override
    public void onEnable() {
        this.saveDefaultConfig();

        boot(this.getConfig().getString("redis.hostname"), this.getConfig().getInt("redis.port"), this.getLogger());

        ServerState online = new ServerState("Online", "§a§lONLINE", true, Material.EMERALD_BLOCK);

        this.getServer().getScheduler().runTaskTimerAsynchronously(this, () -> {
            serverData = new ServerData(
                    this.getConfig().getString("server-id"),
                    this.getServer().getIp() + ":" + this.getServer().getPort(),
                    this.getServer().getOnlinePlayers().size(),
                    this.getServer().getMaxPlayers(),
                    Rank.getById(this.getConfig().getInt("minRank", 0)),
                    online
            );

            try (Jedis jedis = getJedisPool().getResource()) {
                jedis.publish(REDIS_CHANNEL, getGSON().toJson(serverData));
            }
        }, 0, 60);

        database = new MySQL(this, getConfig().getString("MySQL.IP"), getConfig().getString("MySQL.Port"), "server",
                getConfig().getString("MySQL.Username"), getConfig().getString("MySQL.Password"));
        adb = new MySQL(this, getConfig().getString("MySQL.IP"), getConfig().getString("MySQL.Port"), "achievements",
                getConfig().getString("MySQL.Username"), getConfig().getString("MySQL.Password"));

        try {
            connection = database.openConnection();

            getLogger().info("*** CONNECTION TO NETWORK MADE ***");
        } catch (Exception ex) {
            ex.printStackTrace();
            getLogger().warning("*** ERROR CONNECTING TO NETWORK ***");
            getServer().shutdown();
        }

        connection = getConnection();

        try {
            acon = adb.openConnection();

            getLogger().info("*** CONNECTION TO ACHIEVEMENTS MADE ***");
        } catch (Exception ex) {
            ex.printStackTrace();
            getLogger().warning("*** ERROR CONNECTING TO ACHIEVEMENTS ***");
            getServer().shutdown();
        }

        acon = getAcon();

        getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");

        Stream.of(
                new JoinListener(),
                new ChatListener(),
                new AdminListener(),
                new ConnectionListener(),
                new DamageListener(),
                new DeathListener()
        ).forEach(listener -> getServer().getPluginManager().registerEvents(listener, this));

        plugin = this;
        playerManager = new PlayerManager();
        friendManager = new FriendManager();
        freezeManager = new FreezeManager(this);
        commandHandler = new CommandHandler(this);
        achievementHandler = new AchievementHandler();

        Stream.of(
                new AdminModeCommand(),
                new RankCommand(),
                new UUIDCommand(),
                new NamesCommand(),
                new NameCommand(),
                new ShardsCommand(),
                new SoulsCommand(),
                new TempbanCommand(),
                new TempmuteCommand(),
                new BanCommand(),
                new MuteCommand(),
                new KickCommand(),
                new UnbanCommand(),
                new UnmuteCommand(),
                new DebugCommand(),
                new AchievementsCommand()
        ).forEach(command -> commandHandler.addCommand(command));

        achievementHandler.registerAchievement(new Achievement("Test", "GameAPI", "Run /test achievement", 1000));
        achievementHandler.registerAchievement(new Achievement("Test #2", "GameAPI", "You can't get this >:D", 1000));

        getServer().getScheduler().scheduleSyncRepeatingTask(this, new RankParticles(), 0, 20);
    }

    @Override
    public void onDisable() {
        ServerState offline = new ServerState("Offline", "§c§lOFFLINE", false, Material.OBSIDIAN);

        serverData = new ServerData(
                this.getConfig().getString("server-id"),
                this.getServer().getIp() + ":" + this.getServer().getPort(),
                this.getServer().getOnlinePlayers().size(),
                this.getServer().getMaxPlayers(),
                Rank.getById(this.getConfig().getInt("minRank", 0)),
                offline
        );

        try (Jedis jedis = getJedisPool().getResource()) {
            jedis.publish(REDIS_CHANNEL, getGSON().toJson(serverData));
        }

        try {
            connection.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        try {
            acon.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        CustomNPC.despawnAll();
        PlayerNPC.despawnAll();
    }

    public static void boot(String hostname, int port, Logger log) {
        if (JEDIS_POOL != null) {
            return;
        }

        ClassLoader previous = Thread.currentThread().getContextClassLoader();
        Thread.currentThread().setContextClassLoader(Core.class.getClassLoader());
        JEDIS_POOL = new JedisPool(new GenericObjectPoolConfig(), hostname, port, 5000);
        Thread.currentThread().setContextClassLoader(previous);
        GSON = new GsonBuilder().create();

        Thread listenerThread = new Thread(() -> {
            while (true) {
                try {
                    try (Jedis jedis = JEDIS_POOL.getResource()) {
                        jedis.subscribe(new JedisPubSub() {
                            @Override
                            public void onMessage(String channel, String message) {
                                ServerData data = GSON.fromJson(message, ServerData.class);
                                SERVER_CACHE.put(data.getId(), data);
                            }

                            @Override
                            public void onSubscribe(String channel, int subscribedChannels) {
                                log.info("*** Dynamic Server System Started! ***");
                            }

                            @Override
                            public void onUnsubscribe(String channel, int subscribedChannels) {
                                log.info("*** Dynamic Server System Stopped! ***");
                            }
                        }, REDIS_CHANNEL);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        listenerThread.setName("Dynamic Server Config Receiver");
        listenerThread.setDaemon(true);
        listenerThread.start();
    }

    public static JedisPool getJedisPool() {
        return JEDIS_POOL;
    }

    public static Gson getGSON() {
        return GSON;
    }

    public static ServerData getServerData() {
        return serverData;
    }

    public static Map<String, ServerData> getServerCache() {
        return SERVER_CACHE.asMap();
    }

    public static Core getPlugin() {
        return plugin;
    }

    public static PlayerManager getPlayerManager() {
        return playerManager;
    }

    public static FriendManager getFriendManager() {
        return friendManager;
    }

    public static FreezeManager getFreezeManager() {
        return freezeManager;
    }

    public static CommandHandler getCommandHandler() {
        return commandHandler;
    }

    public static AchievementHandler getAchievementHandler() {
        return achievementHandler;
    }

    public static Connection getConnection() {
        return connection;
    }

    public static MySQL getMySQL() {
        return database;
    }

    public static Connection getAcon() {
        return acon;
    }

    public static MySQL getAdb() {
        return adb;
    }

}