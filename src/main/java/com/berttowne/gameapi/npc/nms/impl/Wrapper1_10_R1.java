package com.berttowne.gameapi.npc.nms.impl;

import com.berttowne.gameapi.Core;
import com.berttowne.gameapi.npc.nms.PlayerNPCWrapper;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import net.minecraft.server.v1_10_R1.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_10_R1.CraftServer;
import org.bukkit.craftbukkit.v1_10_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.entity.*;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.UUID;

public class Wrapper1_10_R1 implements PlayerNPCWrapper {

    private EntityPlayer entityPlayer;
    private GameProfile gameProfile;

    @Override
    public Object register(Location location, String uuid, String name) {
        MinecraftServer nmsServer = ((CraftServer) Bukkit.getServer()).getServer();
        WorldServer nmsWorld = ((CraftWorld) location.getWorld()).getHandle();

        gameProfile = makeProfile(name, UUID.fromString(uuid));

        entityPlayer = new EntityPlayer(nmsServer, nmsWorld, gameProfile, new PlayerInteractManager(nmsWorld));
        entityPlayer.playerConnection = new CustomPlayerConnection(entityPlayer.server, new NetworkManager(EnumProtocolDirection.SERVERBOUND), entityPlayer);
        nmsWorld.addEntity(entityPlayer);

        entityPlayer.setLocation(location.getX(), location.getY(), location.getZ(), 0.0f, 0.0f);
        entityPlayer.setCustomName(name);
        entityPlayer.setCustomNameVisible(true);

        for (Player player : Bukkit.getOnlinePlayers()) {
            display(player);
        }

        return entityPlayer;
    }

    @Override
    public void display(Player player) {
        PlayerConnection connection = ((CraftPlayer) player).getHandle().playerConnection;

        connection.sendPacket(new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, entityPlayer));
        connection.sendPacket(new PacketPlayOutNamedEntitySpawn(entityPlayer));

        new BukkitRunnable() {
            @Override
            public void run() {
                connection.sendPacket(new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, entityPlayer));
            }
        }.runTaskLater(Core.getPlugin(),50);
    }

    @Override
    public void despawn() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            PlayerConnection connection = ((CraftPlayer) player).getHandle().playerConnection;

            connection.sendPacket(new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, entityPlayer));
            connection.sendPacket(new PacketPlayOutEntityDestroy(entityPlayer.getId()));
        }
    }

    @Override
    public void lookAt(Location lookat) {
        Location loc = entityPlayer.getBukkitEntity().getLocation().clone();

        double dx = lookat.getX() - loc.getX();
        double dy = lookat.getY() - loc.getY();
        double dz = lookat.getZ() - loc.getZ();

        if (dx != 0) {
            if (dx < 0) {
                loc.setYaw((float) (1.5 * Math.PI));
            } else {
                loc.setYaw((float) (0.5 * Math.PI));
            }
            loc.setYaw(loc.getYaw() - (float) Math.atan(dz / dx));
        } else if (dz < 0) {
            loc.setYaw((float) Math.PI);
        }

        double dxz = Math.sqrt(Math.pow(dx, 2) + Math.pow(dz, 2));

        loc.setPitch((float) -Math.atan(dy / dxz));

        loc.setYaw(-loc.getYaw() * 180f / (float) Math.PI);
        loc.setPitch(loc.getPitch() * 180f / (float) Math.PI);

        PacketPlayOutEntityHeadRotation rotation = new PacketPlayOutEntityHeadRotation(entityPlayer,
                getFixRotation(loc.getYaw()));
        setYaw(loc.getYaw());
        setPitch(loc.getPitch());

        for (Player player : Bukkit.getOnlinePlayers()) {
            PlayerConnection connection = ((CraftPlayer) player).getHandle().playerConnection;

            connection.sendPacket(rotation);
        }
    }

    public byte getFixRotation(float yaw){
        return (byte) ((int) (yaw * 256.0F / 360.0F));
    }

    public void setYaw(float yaw) {
        entityPlayer.yaw = yaw;
    }

    public void setPitch(float pitch) {
        entityPlayer.pitch = pitch;
    }

    private GameProfile makeProfile(String name, UUID skinId) {
        GameProfile profile = new GameProfile(UUID.randomUUID(), name);
        if (skinId != null) {
            GameProfile skin = new GameProfile(skinId, null);
            skin = ((CraftServer) Bukkit.getServer()).getServer().ay().fillProfileProperties(skin, true);
            if (skin.getProperties().get("textures") != null) {
                Property textures = skin.getProperties().get("textures").iterator().next();
                profile.getProperties().put("textures", textures);
            }
        }
        return profile;
    }

    @Override
    public Object getEntity() {
        return entityPlayer.getBukkitEntity();
    }

    public class CustomPlayerConnection extends PlayerConnection {

        public CustomPlayerConnection(MinecraftServer minecraftserver, NetworkManager networkmanager, EntityPlayer entityplayer) {
            super(minecraftserver, networkmanager, entityplayer);
        }

        @Override
        public void sendPacket(Packet packet) {

        }

    }

}