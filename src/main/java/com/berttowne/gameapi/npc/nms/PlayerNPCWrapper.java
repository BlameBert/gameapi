package com.berttowne.gameapi.npc.nms;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public interface PlayerNPCWrapper {

    public Object register(Location location, String uuid, String name);

    public void display(Player player);

    public void despawn();

    public void lookAt(Location location);

    public Object getEntity();

}