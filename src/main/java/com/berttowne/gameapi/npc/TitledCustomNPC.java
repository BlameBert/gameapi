package com.berttowne.gameapi.npc;

import com.berttowne.gameapi.holo.Hologram;
import com.berttowne.gameapi.holo.Version;
import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public abstract class TitledCustomNPC<T extends Entity> extends CustomNPC<T> {

    private Hologram wrapper;

    public TitledCustomNPC(T entity, String... lines) {
        super(entity);

        final Version version = Version.of(Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3]);
        Validate.notNull(version, "Your server version doesn't support Player NPCs!");
        this.wrapper = version.getWrapper();

        wrapper.register(entity.getLocation(), lines);

        for (Player p : Bukkit.getOnlinePlayers()) {
            this.wrapper.display(p);
        }
    }

    public void despawn() {
        for (Player p : Bukkit.getOnlinePlayers()) {
            this.wrapper.destroy(p);
        }

        super.despawn();
    }

}