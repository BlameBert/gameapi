package com.berttowne.gameapi.npc;

import com.berttowne.gameapi.holo.Hologram;
import com.berttowne.gameapi.holo.Version;
import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class TitledPlayerNPC extends PlayerNPC {

    private Hologram wrapper;

    public TitledPlayerNPC(String name, Location location, boolean followPlayers, String... lines) {
        super(name, location, followPlayers);

        final Version version = Version.of(Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3]);
        Validate.notNull(version, "Your server version doesn't support Player NPCs!");
        this.wrapper = version.getWrapper();

        wrapper.register(location, lines);
    }

    protected void onRegister(PlayerNPC playerNPC) {
        for (Player p : Bukkit.getOnlinePlayers()) {
            this.wrapper.display(p);
        }
    }

    protected void onRightClick(PlayerNPC playerNPC, Player player) {

    }

    @Override
    public void despawn(boolean full) {
        for (Player p : Bukkit.getOnlinePlayers()) {
            this.wrapper.destroy(p);
        }

        super.despawn(full);
    }

}