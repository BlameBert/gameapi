package com.berttowne.gameapi.npc;

import com.berttowne.gameapi.Core;
import com.berttowne.gameapi.npc.nms.PlayerNPCWrapper;
import com.berttowne.gameapi.npc.nms.Version;
import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.EquipmentSlot;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class PlayerNPC {

    private static List<PlayerNPC> registeredNPCs = new ArrayList<>();

    private String uuid;
    private String name;
    private Location location;
    private boolean followPlayers;

    private PlayerNPC.PlayerNPCListener listener;

    private boolean targetable = false;

    private PlayerNPCWrapper wrapper;

    private Object entityPlayer;

    public static void despawnAll() {
        Iterator<PlayerNPC> playerNPCIterator = registeredNPCs.iterator();

        while (playerNPCIterator.hasNext()) {
            playerNPCIterator.next().despawn(false);
            playerNPCIterator.remove();
        }
    }

    public PlayerNPC(String name, Location location, boolean followPlayers) {
        this.name = name;
        this.location = location;
        this.followPlayers = followPlayers;

        final Version version = Version.of(Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3]);
        Validate.notNull(version, "Your server version doesn't support Player NPCs!");
        wrapper = version.getWrapper();
    }

    public void register(String uuid) {
        entityPlayer = wrapper.register(location, uuid, name);
        this.uuid = uuid;

        if (this.listener != null) {
            HandlerList.unregisterAll(this.listener);
        }

        PlayerNPCListener listener = new PlayerNPCListener(this);
        Bukkit.getPluginManager().registerEvents(listener, Core.getPlugin());

        this.listener = listener;

        registeredNPCs.add(this);

        onRegister(this);
    }

    public void display(Player player) {
        wrapper.display(player);
    }

    public void despawn(boolean full) {
        if (full) {
            getRegisteredNPCs().removeIf(playerNPC -> playerNPC == this);
        }

        if (this.listener != null) {
            HandlerList.unregisterAll(this.listener);
            this.listener = null;
        }

        wrapper.despawn();
    }

    public Object getEntity() {
        return wrapper.getEntity();
    }

    public boolean doesFollowPlayers() {
        return followPlayers;
    }

    protected List<Entity> getClickableEntities() {
        List<Entity> entities = new ArrayList<>();
        entities.add((Entity) wrapper.getEntity());

        return entities;
    }

    public static List<PlayerNPC> getRegisteredNPCs() {
        return registeredNPCs;
    }

    public boolean isTargetable() {
        return targetable;
    }

    public void setTargetable(boolean targetable) {
        this.targetable = targetable;
    }

    // Listeners
    protected abstract void onRegister(PlayerNPC playerNPC);

    protected abstract void onRightClick(PlayerNPC playerNPC, Player player);

    private class PlayerNPCListener implements Listener {

        private PlayerNPC playerNPC;

        public PlayerNPCListener(PlayerNPC playerNPC) {
            this.playerNPC = playerNPC;
        }

        @EventHandler(ignoreCancelled = true)
        public void onEntityInteract(PlayerInteractEntityEvent event) {
            if (event.getHand() == EquipmentSlot.HAND) {
                if (this.playerNPC.getClickableEntities().contains(event.getRightClicked())) {
                    event.setCancelled(true);
                    this.playerNPC.onRightClick(this.playerNPC, event.getPlayer());
                }
            }
        }

        @EventHandler(ignoreCancelled = true)
        public void onEntityDamage(EntityDamageEvent event) {
            if (this.playerNPC.getClickableEntities().contains(event.getEntity())) {
                event.setCancelled(true);
            }
        }

        @EventHandler(ignoreCancelled = true)
        public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
            if (this.playerNPC.getClickableEntities().contains(event.getEntity())) {
                event.setCancelled(true);
            }
        }

        @EventHandler
        public void onEntityTarget(EntityTargetLivingEntityEvent event) {
            if ((!this.playerNPC.isTargetable()) && (this.playerNPC.getClickableEntities().contains(event.getTarget()))) {
                event.setCancelled(true);
            }
        }

        @EventHandler
        public void onPlayerMove(PlayerMoveEvent event) {
            if (this.playerNPC.doesFollowPlayers()) {
                Entity entity = (Entity) this.playerNPC.wrapper.getEntity();

                double distance = Double.MAX_VALUE;
                Player closest = null;

                for (Entity en : entity.getNearbyEntities(5, 5, 5)) {
                    if (en instanceof Player) {
                        double dist = en.getLocation().distance(entity.getLocation());
                        if (distance == Double.MAX_VALUE || dist < distance) {
                            closest = (Player) en;
                            distance = dist;
                        }
                    }
                }

                if (closest == null) {
                    Location location = entity.getLocation();
                    location.setYaw(0.0f);
                    location.setPitch(0.0f);

                    playerNPC.wrapper.lookAt(location);
                } else {
                    playerNPC.wrapper.lookAt(closest.getLocation());
                }
            }
        }

    }

}