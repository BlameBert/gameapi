package com.berttowne.gameapi.npc;

import com.berttowne.gameapi.Core;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

public abstract class CustomNPC<T extends Entity> {

    private static List<CustomNPC<? extends Entity>> registeredNPCs = new ArrayList<>();
    private T entity;

    private CustomNPC<T>.CustomNPCListener listener;
    private CustomNPC<T>.LocationLock locationLock;

    private boolean targetable = false;

    // Static references
    public static void despawnAll() {
        for (CustomNPC<? extends Entity> npc : registeredNPCs) {
            npc.despawn();
        }
    }

    // Constructor
    public CustomNPC(T entity) {
        if (entity.isDead()) {
            throw new IllegalArgumentException("Entity cannot be dead!");
        }
        this.entity = entity;
    }

    // Methods
    public T getEntity() {
        return this.entity;
    }

    public void register() {
        if (this.listener != null) {
            HandlerList.unregisterAll(this.listener);
        }

        CustomNPCListener listener = new CustomNPCListener(this);
        Bukkit.getPluginManager().registerEvents(listener, Core.getPlugin());

        this.listener = listener;

        registeredNPCs.add(this);

        if (this.entity instanceof Creature) {
            Creature creature = (Creature) this.entity;

            creature.setRemoveWhenFarAway(false);
            creature.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, Integer.MAX_VALUE, 100));
            setLocationLock(creature.getLocation());
        }

        onRegister(this);
    }

    public void despawn() {
        if (this.locationLock != null) {
            this.locationLock.cancel();
            this.locationLock = null;
        }

        if (this.listener != null) {
            HandlerList.unregisterAll(this.listener);
            this.listener = null;
        }

        this.entity.remove();
    }

    protected List<Entity> getClickableEntities() {
        List<Entity> entities = new ArrayList<>();
        entities.add(this.entity);

        return entities;
    }

    public void setLocationLock(Location location) {
        if (this.locationLock != null) {
            this.locationLock.cancel();
        }

        if (location != null) {
            this.locationLock = new LocationLock(this, location);
            this.locationLock.runTaskTimer(Core.getPlugin(), 0L, 5L);
        }
    }

    public Location getLocationLock() {
        return this.locationLock != null ? this.locationLock.getLocation() : null;
    }

    public boolean hasLocationLock() {
        return getLocationLock() != null;
    }

    public boolean isTargetable() {
        return targetable;
    }

    public void setTargetable(boolean targetable) {
        this.targetable = targetable;
    }

    // Listeners
    protected abstract void onRegister(CustomNPC<T> customNPC);

    protected abstract void onRightClick(CustomNPC<T> customNPC, Player player);

    private class CustomNPCListener implements Listener {

        private CustomNPC<T> npc;

        public CustomNPCListener(CustomNPC<T> npc) {
            this.npc = npc;
        }

        @EventHandler(ignoreCancelled = true)
        public void onEntityInteract(PlayerInteractEntityEvent event) {
            if (this.npc.getClickableEntities().contains(event.getRightClicked())) {
                event.setCancelled(true);
                this.npc.onRightClick(this.npc, event.getPlayer());
            }
        }

        @EventHandler(ignoreCancelled = true)
        public void onEntityDamage(EntityDamageEvent event) {
            if (this.npc.getClickableEntities().contains(event.getEntity())){
                event.setCancelled(true);
            }
        }

        @EventHandler(ignoreCancelled = true)
        public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
            if (this.npc.getClickableEntities().contains(event.getEntity())){
                event.setCancelled(true);
            }
        }

        @EventHandler
        public void onEntityTarget(EntityTargetLivingEntityEvent event) {
            if ((!this.npc.isTargetable()) && (this.npc.getClickableEntities().contains(event.getTarget()))) {
                event.setCancelled(true);
            }
        }

    }

    // Stay still
    private class LocationLock extends BukkitRunnable {

        private Location loc;
        private CustomNPC<T> npc;

        public LocationLock(CustomNPC<T> npc, Location loc) {
            this.npc = npc;
            this.loc = loc;
        }

        public void run() {
            if (this.npc.getEntity().getLocation().distanceSquared(this.getLocation()) > 0.25D) {
                Location toTeleport = this.loc.clone();

                toTeleport.setPitch(this.npc.getEntity().getLocation().getPitch());
                toTeleport.setYaw(this.npc.getEntity().getLocation().getYaw());

                this.npc.getEntity().teleport(toTeleport);
            }
        }

        public Location getLocation() {
            return loc;
        }

    }

}