package com.berttowne.gameapi.gui;

import com.berttowne.gameapi.Core;
import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

public abstract class GUI {

    protected static final ItemStack SPACER = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 0, Byte.valueOf(DyeColor.BLACK.getData()));
    protected InventoryProxy inventory;

    static {
        ItemMeta itemMeta = SPACER.getItemMeta();
        itemMeta.setDisplayName(" ");
        SPACER.setItemMeta(itemMeta);
    }

    private Inventory bukkitInventory;

    private boolean populated = false;
    private boolean invCheckOverride = false;
    private boolean allowDrag = false;
    private boolean allowShiftClicking = false;

    private BukkitRunnable updaterTask;

    public GUI(String title, int size) {
        this.bukkitInventory = Bukkit.createInventory(null, getInvSizeForCount(size), title);
        this.inventory = new InventoryProxy(this.bukkitInventory, Bukkit.createInventory(null, getInvSizeForCount(size), title));
        Bukkit.getPluginManager().registerEvents(new GUIEvents(this), Core.getPlugin());
    }

    public GUI(String title, InventoryType type) {
        this.bukkitInventory = Bukkit.createInventory(null, type, title);
        this.inventory = new InventoryProxy(this.bukkitInventory, Bukkit.createInventory(null, type, title));
        Bukkit.getPluginManager().registerEvents(new GUIEvents(this), Core.getPlugin());
    }

    public GUI(Inventory bukkitInventory) {
        this.bukkitInventory = bukkitInventory;
        this.inventory = new InventoryProxy(bukkitInventory, bukkitInventory);
        Bukkit.getPluginManager().registerEvents(new GUIEvents(this), Core.getPlugin());
    }

    protected final int getInvSizeForCount(int count) {
        int size = count / 9 * 9;
        if (count % 9 > 0) size += 9;
        if (size < 9) return 9;
        if (size > 54) return 54;
        return size;
    }

    public final void open(Player player) {
        try {
            if (!this.populated) {
                populate();
                this.inventory.apply();
                this.populated = true;
            }
            openInventory(player);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public final void close() {
        for (HumanEntity human : getBukkitInventory().getViewers()) {
            human.closeInventory();
        }
    }

    protected final void repopulate() {
        try {
            this.inventory.clear();
            populate();
            this.inventory.apply();
            this.populated = true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    protected final void setUpdateTicks(int ticks) {
        setUpdateTicks(ticks, false);
    }

    protected final void setUpdateTicks(int ticks, boolean sync) {
        if (this.updaterTask != null) {
            this.updaterTask.cancel();
            this.updaterTask = null;
        }

        this.updaterTask = new GUIUpdateTask(this);
        if (sync) {
            this.updaterTask.runTaskTimer(Core.getPlugin(), 0L, ticks);
        } else {
            this.updaterTask.runTaskTimerAsynchronously(Core.getPlugin(), 0L, ticks);
        }
    }

    protected final void scheduleOpen(final GUI gui, final Player player) {
        Bukkit.getScheduler().runTask(Core.getPlugin(), new Runnable() {
            public void run() {
                gui.open(player);
            }
        });
    }

    public void setInvCheckOverride(boolean invCheckOverride) {
        this.invCheckOverride = invCheckOverride;
    }

    protected void openInventory(Player player) {
        player.openInventory(getBukkitInventory());
    }

    public Inventory getBukkitInventory() {
        return bukkitInventory;
    }

    public boolean isAllowShiftClicking() {
        return allowShiftClicking;
    }

    public void setAllowShiftClicking(boolean allowShiftClicking) {
        this.allowShiftClicking = allowShiftClicking;
    }

    public boolean isAllowDrag() {
        return allowDrag;
    }

    public void setAllowDrag(boolean allowDrag) {
        this.allowDrag = allowDrag;
    }

    protected void onPlayerCloseInv() {}

    protected void onPlayerDrag(InventoryDragEvent event) {}

    protected abstract void onPlayerClick(InventoryClickEvent event);

    protected abstract void populate();

    protected void onTickUpdate() {}

    private class GUIEvents implements Listener {

        private GUI gui;

        public GUIEvents(GUI gui) {
            this.gui = gui;
        }

        @EventHandler
        public void onInventoryClick(InventoryClickEvent event) {
            if (this.gui.getBukkitInventory().getViewers().contains(event.getWhoClicked())) {
                List<InventoryAction> deniedActions = new LinkedList<>(Arrays.asList(InventoryAction.CLONE_STACK, InventoryAction.COLLECT_TO_CURSOR, InventoryAction.UNKNOWN));

                if (!GUI.this.allowShiftClicking) {
                    deniedActions.add(InventoryAction.MOVE_TO_OTHER_INVENTORY);
                }

                if (deniedActions.contains(event.getAction())) {
                    event.setCancelled(true);
                }

                if ((!GUI.this.allowShiftClicking) && (event.getClick().isShiftClick())) {
                    event.setCancelled(true);
                }

            }

            if ((!GUI.this.invCheckOverride) && ((event.getClickedInventory() == null) || (!event.getClickedInventory().equals(this.gui.getBukkitInventory()))))
                return;

            event.setCancelled(true);

            if (!(event.getWhoClicked() instanceof Player)) return;

            try {
                this.gui.onPlayerClick(event);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        @EventHandler
        public void onInventoryClose(InventoryCloseEvent event) {
            if (!event.getInventory().equals(this.gui.getBukkitInventory())) return;

            if (GUI.this.getBukkitInventory().getViewers().size() <= 1) {
                HandlerList.unregisterAll(this);

                try {
                    this.gui.onPlayerCloseInv();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                if (this.gui.updaterTask != null) {
                    this.gui.updaterTask.cancel();
                }
            }
        }

        @EventHandler
        public void onInventoryDrag(InventoryDragEvent event) {
            if (!event.getInventory().equals(this.gui.getBukkitInventory())) return;

            if (!GUI.this.allowDrag) {
                event.setCancelled(true);
            } else {
                try {
                    this.gui.onPlayerDrag(event);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

    }

    private class GUIUpdateTask extends BukkitRunnable {

        private GUI gui;

        public GUIUpdateTask(GUI gui) {
            this.gui = gui;
        }

        public void run() {
            this.gui.repopulate();

            try {
                this.gui.onTickUpdate();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }

    public class InventoryProxy implements Inventory {

        private Inventory mainInventory;
        private Inventory proxyInventory;

        private InventoryProxy(Inventory mainInventory, Inventory proxyInventory) {
            this.mainInventory = mainInventory;
            this.proxyInventory = proxyInventory;
        }

        public void apply() {
            this.mainInventory.setContents(this.proxyInventory.getContents());
        }

        public int getSize() {
            return this.proxyInventory.getSize();
        }

        public int getMaxStackSize() {
            return this.proxyInventory.getMaxStackSize();
        }

        public void setMaxStackSize(int i) {
            this.proxyInventory.setMaxStackSize(i);
        }

        public String getName() {
            return this.proxyInventory.getName();
        }

        public ItemStack getItem(int i) {
            return this.proxyInventory.getItem(i);
        }

        public void setItem(int i, ItemStack itemStack) {
            this.proxyInventory.setItem(i, itemStack);
        }

        public HashMap<Integer, ItemStack> addItem(ItemStack... itemStacks) throws IllegalArgumentException {
            return this.proxyInventory.addItem(itemStacks);
        }

        public HashMap<Integer, ItemStack> removeItem(ItemStack... itemStacks) throws IllegalArgumentException {
            return this.proxyInventory.removeItem(itemStacks);
        }

        public ItemStack[] getContents() {
            return this.proxyInventory.getContents();
        }

        public void setContents(ItemStack[] itemStacks) throws IllegalArgumentException {
            this.proxyInventory.setContents(itemStacks);
        }

        public ItemStack[] getStorageContents() {
            return this.proxyInventory.getStorageContents();
        }

        public void setStorageContents(ItemStack[] itemStacks) throws IllegalArgumentException {
            this.proxyInventory.setStorageContents(itemStacks);
        }

        public boolean contains(int i) {
            return this.proxyInventory.contains(i);
        }

        public boolean contains(Material material) throws IllegalArgumentException {
            return this.proxyInventory.contains(material);
        }

        public boolean contains(ItemStack itemStack) {
            return this.proxyInventory.contains(itemStack);
        }

        public boolean contains(int i, int i1) {
            return this.proxyInventory.contains(i, i1);
        }

        public boolean contains(Material material, int i) throws IllegalArgumentException {
            return this.proxyInventory.contains(material, i);
        }

        public boolean contains(ItemStack itemStack, int i) {
            return this.proxyInventory.contains(itemStack, i);
        }

        public boolean containsAtLeast(ItemStack itemStack, int i) {
            return this.proxyInventory.containsAtLeast(itemStack, i);
        }

        public HashMap<Integer, ? extends ItemStack> all(int i) {
            return this.proxyInventory.all(i);
        }

        public HashMap<Integer, ? extends ItemStack> all(Material material) throws IllegalArgumentException {
            return this.proxyInventory.all(material);
        }

        public HashMap<Integer, ? extends ItemStack> all(ItemStack itemStack) {
            return this.proxyInventory.all(itemStack);
        }

        public int first(int i) {
            return this.proxyInventory.first(i);
        }

        public int first(Material material) throws IllegalArgumentException {
            return this.proxyInventory.first(material);
        }

        public int first(ItemStack itemStack) {
            return this.proxyInventory.first(itemStack);
        }

        public int firstEmpty() {
            return this.proxyInventory.firstEmpty();
        }

        public void remove(int i) {
            this.proxyInventory.remove(i);
        }

        public void remove(Material material) throws IllegalArgumentException {
            this.proxyInventory.remove(material);
        }

        public void remove(ItemStack itemStack) {
            this.proxyInventory.remove(itemStack);
        }

        public void clear(int i) {
            this.proxyInventory.clear(i);
        }

        public void clear() {
            this.proxyInventory.clear();
        }

        public List<HumanEntity> getViewers() {
            return this.mainInventory.getViewers();
        }

        public String getTitle() {
            return this.mainInventory.getTitle();
        }

        public InventoryType getType() {
            return this.mainInventory.getType();
        }

        public InventoryHolder getHolder() {
            return this.mainInventory.getHolder();
        }

        public ListIterator<ItemStack> iterator() {
            return this.proxyInventory.iterator();
        }

        public ListIterator<ItemStack> iterator(int i) {
            return this.proxyInventory.iterator(i);
        }

        public Location getLocation() {
            return this.proxyInventory.getLocation();
        }

    }

}