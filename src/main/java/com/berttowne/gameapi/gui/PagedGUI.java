package com.berttowne.gameapi.gui;

import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public abstract class PagedGUI extends GUI {

    private int page;
    private int headerRows;
    private final int internalSize;

    public PagedGUI(String title, int size) {
        super(title, size);

        this.internalSize = getInvSizeForCount(size - 9);
    }

    protected abstract List<ItemStack> getIcons();

    protected abstract void onPlayerClickIcon(InventoryClickEvent paramInventoryClickEvent);

    protected abstract void populateSpecial();

    public void setHeaderRows(int headerRows) {
        this.headerRows = headerRows;
    }

    protected final void onPlayerClick(InventoryClickEvent event) {
        List<ItemStack> items = getIcons();

        if (items == null) {
            items = new ArrayList<>();
        }

        int pageSize = this.internalSize - this.headerRows * 9;
        int pages = items.size() / pageSize;

        if (items.size() % pageSize > 0) {
            pages++;
        }

        if ((event.getRawSlot() == this.internalSize + 3) && (this.page > 0)) {
            this.page -= 1;
            repopulate();
            return;
        }

        if ((event.getRawSlot() == this.internalSize + 5) && (this.page + 1 < pages)) {
            this.page += 1;
            repopulate();
            return;
        }

        onPlayerClickIcon(event);
    }

    protected final void populate() {
        List<ItemStack> items = getIcons();

        if (items == null) {
            items = new ArrayList<>();
        }

        int pageSize = this.internalSize - this.headerRows * 9;
        int pages = items.size() / pageSize;

        if (items.size() % pageSize > 0) {
            pages++;
        }

        if (this.page > pages) {
            this.page -= 1;
            repopulate();
            return;
        }

        int slot = this.headerRows * 9;

        for (int i = this.page * pageSize; i < items.size(); i++) {
            if (slot > pageSize + this.headerRows * 9 - 1)
                break;

            this.inventory.setItem(slot++, items.get(i));
        }

        if (this.page > 0) {
            ItemStack arrow = new ItemStack(Material.ARROW);
            ItemMeta arrowMeta = arrow.getItemMeta();
            arrowMeta.setDisplayName("§7§l§o<- §fPrevious Page");
            arrow.setItemMeta(arrowMeta);
            this.inventory.setItem(this.internalSize + 3, arrow);
        }

        if (this.page + 1 < pages) {
            ItemStack arrow = new ItemStack(Material.ARROW);
            ItemMeta arrowMeta = arrow.getItemMeta();
            arrowMeta.setDisplayName("§fNext Page §7§l§o->");
            arrow.setItemMeta(arrowMeta);
            this.inventory.setItem(this.internalSize + 5, arrow);
        }

        ItemStack pageNumber = new ItemStack(Material.EMPTY_MAP);
        ItemMeta pageNumberMeta = pageNumber.getItemMeta();
        pageNumberMeta.setDisplayName("§fPage §7" + (this.page + 1) + "§8/§7" + pages);
        pageNumber.setItemMeta(pageNumberMeta);
        this.inventory.setItem(this.internalSize + 4, pageNumber);

        populateSpecial();
    }

}